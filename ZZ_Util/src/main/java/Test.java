import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.GZIPInputStream;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import jtw.util.gui.SpringJMenuInitializer;
import jtw.util.gui.Tree;
import jtw.util.io.Copy;

public class Test {

	/**
	 * @throws IOException ********************************/
	public static void main(String[] args) throws IOException {
		yoyoy();
	}
	
	
	private static void yyyyy() throws IOException {
		File file = new File("C:\\Users\\Besitzer\\AppData\\Local\\Temp\\jan1191370561986615532test.gzip");
		try (BufferedInputStream input = new BufferedInputStream(new GZIPInputStream(new FileInputStream(file)));
				BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
		}
	}
	
	private static void yoyoy() throws IOException {
		HttpURLConnection.setFollowRedirects(true);
		
		URL url = new URL("https://api.nasdaq.com/api/quote/ABNB/historical?assetclass=stocks&fromdate=2011-11-28&limit=9999&todate=2021-11-28");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setConnectTimeout(15 * 1000);
		connection.setReadTimeout(15 * 1000);
		connection.setAllowUserInteraction(false);
		connection.setRequestMethod("GET");
	    connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36");
	    connection.setRequestProperty("accept", "text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8");
	    connection.setRequestProperty("accept-encoding", "gzip, deflate, br");
	    connection.connect();
	    
	    int response = connection.getResponseCode();
	    if (response >= 0) {
			File tmpFile = File.createTempFile("jan", "test");
			tmpFile.deleteOnExit();
			System.out.println(tmpFile.getAbsolutePath());
			
		    try (InputStream input = new BufferedInputStream(connection.getInputStream());
		    		OutputStream output = new BufferedOutputStream(new FileOutputStream(tmpFile))) {
		    	Copy.copy(input, output);
		    }
	    }
		
//		Copy.copyFromURL("https://api.nasdaq.com/api/quote/ABNB/historical?assetclass=stocks&fromdate=2011-11-28&limit=9999&todate=2021-11-28", tmpFile);
		
	}
	
	private static void bla() {
		int internalTempPpuAddress = 0;
		int value = 0xFFFF;
		internalTempPpuAddress = (internalTempPpuAddress & 0x18FF) | ((value & 0x00F8) << 2) | ((value & 0x0007) << 12);
		System.out.println();
	}
	
	private static void testBitUtil() {
		
	}
	
	private static void someTest() {
		JFrame frame = new JFrame("Tree Test");
		frame.setSize(800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Action action = new AbstractAction("a") {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("I have been clicked");
				putValue(Action.SELECTED_KEY, Boolean.TRUE);
			}
		};
		
		JMenuBar bar = new JMenuBar();
		JMenu menu = new JMenu("abc");
		JCheckBoxMenuItem a = new JCheckBoxMenuItem("a");
		a.setAction(action);
		JCheckBoxMenuItem b = new JCheckBoxMenuItem("b");
		b.setAction(action);
		menu.add(a);
		menu.add(b);
		bar.add(menu);
		frame.setJMenuBar(bar);
		
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				action.putValue(Action.SELECTED_KEY, Boolean.TRUE);
			}
		});
		t.start();

		frame.setVisible(true);
	}
	
	public static void main3(String[] args) throws Exception {
		Path currentRelativePath = Paths.get("");
		String s = currentRelativePath.toAbsolutePath().toString();
		System.out.println("Current relative path is: " + s);
		
		File f = new File("resources/menu.xml");
		System.out.println(f.exists());
		SpringJMenuInitializer minit = new SpringJMenuInitializer();
		JMenuBar bar = minit.getJMenuBar("resources/menu.xml");

		JFrame frame = new JFrame("Tree Test");
		frame.setSize(800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setJMenuBar(bar);

		frame.setVisible(true);
	}

	public static void main2(String[] args) {
		JFrame frame = new JFrame("Tree Test");
		frame.setSize(800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel p = new JPanel(new BorderLayout());

		DefaultMutableTreeNode root = new DefaultMutableTreeNode("root");
		JTree tree = new JTree(root);
		tree.setEditable(true);
		tree.setCellRenderer(new Tree());

		JPanel tt = getTestPanel();

		root.add(new DefaultMutableTreeNode("child"));
		root.add(new DefaultMutableTreeNode(tt));
		tree.setExpandsSelectedPaths(true);

		p.add(tree, BorderLayout.CENTER);

		frame.add(p);

		// frame.pack();
		frame.setVisible(true);
	}

	private static JPanel getTestPanel() {
		JPanel p = new JPanel();
		JButton ttbutton = new JButton("TT");
		ttbutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("TT button pressed");
			}
		});
		p.add(ttbutton);
		p.add(new JLabel("abc"));
		return p;
	}

	public int testing() {
		int x = 0;
		for (int i = 0; i < 10; i++) {
			x++;
		}
		return x;
	}

}
