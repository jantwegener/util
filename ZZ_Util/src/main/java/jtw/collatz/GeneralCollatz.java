package jtw.collatz;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GeneralCollatz {
	
	public static void main(String[] args) {
		for (int x = 61; x <= 80; x++) {
			List<Integer> path = collatz(x);
			
			System.out.println(path);
		}
	}
	
	public static List<Integer> collatz(int x) {
		List<Integer> path = new ArrayList<>();
		Set<Integer> visitedSet = new HashSet<>();
		
		while (x > 1 && !visitedSet.contains(x)) {
			visitedSet.add(x);
			path.add(x);
			
			x = f(x);
		}
		path.add(x);
		
		return path;
	}
	
	public static int f(int x) {
		final int ret;
		if (x % 2 == 0) {
			ret = x / 2;
		} else {
			ret = 3 * x - 1;
		}
		
		return ret;
	}
	
}
