package jtw.genetic;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;

import jtw.util.rnd.RND;

public class Gene implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private byte[] b;
	
	public Gene(double ... d) {
		ByteBuffer buffer = ByteBuffer.allocate(d.length * 8);
		DoubleBuffer doubleView = buffer.asDoubleBuffer();
		doubleView.put(d);
		b = buffer.array();
	}
	
	private Gene(int size) {
		b = new byte[size];
	}
	
	private Gene(byte[] bb) {
		this(bb.length);
		System.arraycopy(bb, 0, b, 0, bb.length);
	}
	
	public double[] asDoubleArray() {
		ByteBuffer bb = ByteBuffer.wrap(b);
		double[] dst = new double[b.length / 8];
		bb.asDoubleBuffer().get(dst);
		
		return dst;
	}
	
	public Gene crossOver(Gene g) {
		int cut = RND.nextInt(1, b.length - 1);
		Gene tmp = new Gene(g.b.length);
		
		System.arraycopy(b, 0, tmp.b, 0, cut);
		System.arraycopy(g.b, cut, tmp.b, cut, tmp.b.length - cut);
		
		return tmp;
	}
	
	public Gene mutate(double mutationRate) {
		for (int i = 0; i < b.length; i++) {
			byte xor = 0;
			for (int j = 0; j < 8; j++) {
				if (RND.nextDouble() < mutationRate) {
					xor |= 1 << j;
				}
			}
			b[i] = (byte)(b[i] ^ xor);
		}
		Gene tmp = new Gene(b);
		return tmp;
	}
	
}
