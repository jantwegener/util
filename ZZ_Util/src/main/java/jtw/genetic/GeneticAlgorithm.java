package jtw.genetic;

import java.util.ArrayList;
import java.util.List;
import java.util.NavigableSet;
import java.util.TreeSet;

import jtw.math.DFunction;
import jtw.neuralnetwork.NeuralNetwork;
import jtw.util.rnd.RND;

public class GeneticAlgorithm {

	private double survivalRate;
	private double mutationRate;

	private NeuralNetwork[] population;
	
	NeuralNetwork bestNN = null;

	public GeneticAlgorithm(int populationSize, double survivalRate, double mutationRate) {
		population = new NeuralNetwork[populationSize];

		this.survivalRate = survivalRate;
		this.mutationRate = mutationRate;
	}

	public void initRandom(DFunction f, int ... layers) {
		for (int i = 0; i < population.length; i++) {
			NeuralNetwork nn = new NeuralNetwork(f, layers);
			nn.initRandom();
			population[i] = nn;
		}
	}
	
	public NeuralNetwork getBest() {
		return bestNN;
	}

	private double step(TestSet[] testSet) {
		NavigableSet<NetworkDistance> valuedPopulationSet = new TreeSet<>();
		for (NeuralNetwork nn : population) {
			double dist = 0d;
			for (TestSet set : testSet) {
				dist += set.test(nn);
			}
			NetworkDistance nd = new NetworkDistance(dist, nn);
			valuedPopulationSet.add(nd);
		}

		// search for survivors
		List<NeuralNetwork> survivalPopulationList = new ArrayList<>();
		int survivors = (int) Math.max(2, Math.ceil(population.length * survivalRate));
		// already put the first survivor into the next population
		NetworkDistance best = valuedPopulationSet.pollFirst();
		bestNN = best.nn;
		population[0] = bestNN;
		survivalPopulationList.add(bestNN);
		for (int i = 1; i < survivors; i++) {
			NetworkDistance nd = valuedPopulationSet.pollFirst();
			survivalPopulationList.add(nd.nn);
		}

		// populate the next generation
		for (int i = 1; i < population.length; i++) {
			int father = 0;
			int mother = 0;
			while (father == mother) {
				father = RND.nextInt(0, survivors);
				mother = RND.nextInt(0, survivors);
			}
			
			NeuralNetwork fatherNN = survivalPopulationList.get(father);
			NeuralNetwork motherNN = survivalPopulationList.get(mother);
			
			Gene fatherGene = new Gene(fatherNN.getNetworkAsArray());
			Gene motherGene = new Gene(motherNN.getNetworkAsArray());
			Gene childGene = fatherGene.crossOver(motherGene);
			childGene = childGene.mutate(mutationRate);
			
			NeuralNetwork child = new NeuralNetwork(fatherNN, childGene.asDoubleArray());
			population[i] = child;
		}
		
		return best.dist;
	}

	public void train(TestSet[] testSet, int maxIteration, double maxDist) {
		// initRandom();
		
		int iteration = 0;
		double dist = Double.MAX_VALUE;
		while (maxDist < dist && iteration++ < maxIteration) {
			dist = step(testSet);
			System.out.println("dist: " + dist);
		}
	}

	private class NetworkDistance implements Comparable<NetworkDistance> {
		private final double dist;
		private final NeuralNetwork nn;

		public NetworkDistance(double dist, NeuralNetwork nn) {
			this.dist = dist;
			this.nn = nn;
		}

		@Override
		public int compareTo(NetworkDistance o) {
			if (this == o) {
				return 0;
			}
			int ret = Double.compare(dist, o.dist);
			if (ret == 0) {
				ret = Integer.compare(hashCode(), o.hashCode());
			}
			return ret;
		}
	}

}
