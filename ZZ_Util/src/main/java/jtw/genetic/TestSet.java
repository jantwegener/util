package jtw.genetic;

import java.io.Serializable;

import jtw.neuralnetwork.NeuralNetwork;

public class TestSet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private double[] input;
	// the expected output
	private double[] output;
	
	public TestSet(double[] input, double[] output) {
		this.input = input;
		this.output = output;
	}

	public double test(NeuralNetwork nn) {
		double[] out = nn.feedForward(input);
		
		double value = 0d;
		for (int i = 0; i < output.length; i++) {
			value += (out[i] - output[i]) * (out[i] - output[i]);
		}
		
		return value;
	}
	
}
