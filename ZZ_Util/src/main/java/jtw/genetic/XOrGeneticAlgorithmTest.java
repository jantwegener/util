package jtw.genetic;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JPanel;

import jtw.math.SigmoidFunction;
import jtw.neuralnetwork.NeuralNetwork;

public class XOrGeneticAlgorithmTest {

	public static void main(String[] args) {
		TestSet[] testSet = new TestSet[4];
		testSet[0] = new TestSet(new double[] { 1, 1 }, new double[] { 0 });
		testSet[1] = new TestSet(new double[] { 0, 1 }, new double[] { 1 });
		testSet[2] = new TestSet(new double[] { 1, 0 }, new double[] { 1 });
		testSet[3] = new TestSet(new double[] { 0, 0 }, new double[] { 0 });

//		GeneticAlgorithm algo = new GeneticAlgorithm(200, 0.08, 0.1);
//		algo.initRandom(new SigmoidFunction(), 2, 2, 1);
		GeneticAlgorithm algo = new GeneticAlgorithm(200, 0.1, 0.2);
		algo.initRandom(new SigmoidFunction(), 2, 15, 18, 115, 1);

		algo.train(testSet, 5000, 1E-6);

		NeuralNetwork nn = algo.getBest();
		System.out.println(nn);
		System.out.println("--");

		System.out.println(Arrays.toString(nn.feedForward(1, 1)));
		System.out.println(Arrays.toString(nn.feedForward(1, 0)));
		System.out.println(Arrays.toString(nn.feedForward(0, 1)));
		System.out.println(Arrays.toString(nn.feedForward(0, 0)));

		int size = 1000;
		final BufferedImage theImage = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);

		for (double y = 0; y < 1; y += (1d / size)) {
			for (double x = 0; x < 1; x += (1d / size)) {
				int color = (int) (nn.feedForward(x, y)[0] * 255);
				Color c = new Color(color, color, color);
				theImage.setRGB((int) (x * size), (int) (y * size), c.getRGB());
			}
		}

		JFrame frame = new JFrame();
		frame.setSize(400, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel imgPanel = new JPanel() {
			static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g) {
				g.drawImage(theImage, 0, 0, getWidth(), getHeight(), null);
			}
		};
		frame.add(imgPanel);
		frame.setVisible(true);
	}

}
