package jtw.graph;

public class DirectedArc<N, T> {
	
	private Node<N> startNode;
	private Node<N> endNode;
	
	private double weight;

	private T datum;

	public DirectedArc() {
	}

	public DirectedArc(Node<N> startNode, Node<N> endNode, double weight) {
		super();
		this.startNode = startNode;
		this.endNode = endNode;
		this.weight = weight;
	}

	public Node<N> getStartNode() {
		return startNode;
	}

	public void setStartNode(Node<N> startNode) {
		this.startNode = startNode;
	}

	public Node<N> getEndNode() {
		return endNode;
	}

	public void setEndNode(Node<N> endNode) {
		this.endNode = endNode;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public T getDatum() {
		return datum;
	}

	public void setDatum(T datum) {
		this.datum = datum;
	}

	@Override
	public String toString() {
		return "DirectedArc [startNode=" + startNode + ", endNode=" + endNode + ", weight=" + weight + ", datum="
				+ datum + "]";
	}

}
