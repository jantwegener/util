package jtw.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import jtw.graph.algorithm.NodeNotFoundException;

import java.util.Collections;

public class Graph<N, T> {

	private Set<Node<N>> nodeSet = new HashSet<>(); 

	private Set<DirectedArc<N, T>> arcSet = new HashSet<>();

	private HashMap<Node<N>, Set<DirectedArc<N, T>>> outgoingMap = new HashMap<>();

	private HashMap<Node<N>, Set<DirectedArc<N, T>>> incomingMap = new HashMap<>();

	public Graph() {
	}

	public Set<DirectedArc<N, T>> getOutgoingSet(Node<N> node) {
		Set<DirectedArc<N, T>> outgoingArcSet = outgoingMap.get(node);
		if (outgoingArcSet == null) {
			outgoingArcSet = Collections.emptySet();
		}
		return outgoingArcSet;
	}

	public Set<DirectedArc<N, T>> getIncomingSet(Node<N> node) {
		Set<DirectedArc<N, T>> incomingArcSet = incomingMap.get(node);
		if (incomingArcSet == null) {
			incomingArcSet = Collections.emptySet();
		}
		return incomingArcSet;
	}
	
	public void addNode(Node<N> node) {
		nodeSet.add(node);
	}
	
	public boolean containsNode(Node<N> node) {
		return nodeSet.contains(node);
	}

	public void addArc(DirectedArc<N, T> arc) {
		arcSet.add(arc);
		nodeSet.add(arc.getStartNode());
		nodeSet.add(arc.getEndNode());
		
		Set<DirectedArc<N, T>> outgoingSet = outgoingMap.get(arc.getStartNode());
		if (outgoingSet == null) {
			outgoingSet = new HashSet<>();
			outgoingMap.put(arc.getStartNode(), outgoingSet);
		}
		outgoingSet.add(arc);
		
		Set<DirectedArc<N, T>> ingoingSet = incomingMap.get(arc.getEndNode());
		if (ingoingSet == null) {
			ingoingSet = new HashSet<>();
			incomingMap.put(arc.getEndNode(), ingoingSet);
		}
		ingoingSet.add(arc);
	}
	
	public Node<N> getNode(String nodeName) throws NodeNotFoundException {
		for (Node<N> n : nodeSet) {
			if (nodeName.equals(n.getName())) {
				return n;
			}
		}
		throw new NodeNotFoundException("Node '" + nodeName + "' is not in the graph.");
	}

	public Set<Node<N>> getNodeSet() {
		return nodeSet;
	}

	public void setNodeSet(Set<Node<N>> nodeSet) {
		this.nodeSet = nodeSet;
	}

	public Set<DirectedArc<N, T>> getArcSet() {
		return arcSet;
	}

	public void setArcSet(Set<DirectedArc<N, T>> arcSet) {
		this.arcSet = arcSet;
	}

}
