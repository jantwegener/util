package jtw.graph;

import java.util.Objects;

public class Node<T> {

	private final String name;

	private T datum;

	/**
	 * Creates a new node with the given name. The name serves as id and must be unique within the graph.
	 * 
	 * @param name The name of the node.
	 */
	public Node(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public T getDatum() {
		return datum;
	}

	public void setDatum(T datum) {
		this.datum = datum;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node<?> other = (Node<?>) obj;
		return Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return "Node [name=" + name + ", datum=" + datum + "]";
	}

}
