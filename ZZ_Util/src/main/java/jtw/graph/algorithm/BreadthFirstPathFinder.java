package jtw.graph.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import jtw.graph.DirectedArc;
import jtw.graph.Graph;
import jtw.graph.Node;

public class BreadthFirstPathFinder<N, T> implements PathFinder<N, T> {

	public BreadthFirstPathFinder() {
	}

	@Override
	public List<DirectedArc<N, T>> findPath(Graph<N, T> graph, Node<N> startNode, Node<N> endNode) throws NodeNotFoundException {
		if (!graph.containsNode(startNode) || !graph.containsNode(endNode)) {
			throw new NodeNotFoundException("Could not find start or end node.");
		}

		List<DirectedArc<N, T>> path = new ArrayList<>();
		Set<Node<N>> visitedNodeSet = new HashSet<>();
		visitedNodeSet.add(startNode);

		Map<Node<N>, DirectedArc<N, T>> parentArcMap = new HashMap<>();
		Queue<Node<N>> nodeQueue = new LinkedList<>();

		nodeQueue.add(startNode);

		while (!nodeQueue.isEmpty()) {
			Node<N> nextNode = nodeQueue.poll();
			if (nextNode.equals(endNode)) {
				Node<N> backwardsNode = endNode;
				while (!backwardsNode.equals(startNode)) {
					DirectedArc<N, T> backwardsArc = parentArcMap.get(backwardsNode);
					path.add(backwardsArc);
					backwardsNode = backwardsArc.getStartNode();
				}
				Collections.reverse(path);
			} else {
				for (DirectedArc<N, T> arc : graph.getOutgoingSet(nextNode)) {
					Node<N> nextToVisitNode = arc.getEndNode();
					if (!visitedNodeSet.contains(nextToVisitNode)) {
						visitedNodeSet.add(nextToVisitNode);
						parentArcMap.put(nextToVisitNode, arc);
						nodeQueue.add(nextToVisitNode);
					}
				}
			}
		}

		return path;
	}

}
