package jtw.graph.algorithm;

import java.util.List;

import jtw.graph.DirectedArc;
import jtw.graph.Graph;
import jtw.graph.Node;

public interface PathFinder<N, T> {

	/**
	 * Searches a path from startNode to endNode and returns a path between the two nodes.
	 * 
	 * @param graph
	 * @param startNode
	 * @param endNode
	 * 
	 * @return A path between the two nodes or an empty list if none exists.
	 * 
	 * @throws NodeNotFoundException If either the start or end node are not in the graph.
	 */
	public List<DirectedArc<N, T>> findPath(Graph<N, T> graph, Node<N> startNode, Node<N> endNode) throws NodeNotFoundException;

}
