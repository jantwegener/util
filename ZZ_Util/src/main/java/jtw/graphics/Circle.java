package jtw.graphics;

import java.awt.geom.Point2D;

public class Circle {

	private Point2D centerPoint = new Point2D.Double(0, 0);
	private double radius = 0d;
	
	public Circle() {
	}
	
	public Circle(double x, double y, double r) {
		centerPoint.setLocation(x, y);
		radius = r;
	}
	
	public Point2D getCenter() {
		return centerPoint;
	}
	
	public void setCenter(double x, double y) {
		centerPoint.setLocation(x, y);
	}
	
	public void setCenter(Point2D p) {
		centerPoint.setLocation(p);
	}
	
	public double getRadius() {
		return radius;
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public boolean intersects(Circle c) {
		return centerPoint.distance(c.centerPoint) <= radius + c.radius;
	}
	
}
