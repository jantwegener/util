package jtw.graphics;

import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Paint;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;
import java.text.AttributedCharacterIterator;
import java.util.Map;

public class JGraphics2D extends Graphics2D {
	
	private Graphics2D innerGraphics;
	
	/**
	 * Creates a new {@link JGraphics2D} object from the given {@link Graphics2D} object. The {@link Graphics2D} object is used to perform the operations.
	 * 
	 * @param g The graphics to paint with.
	 */
	public JGraphics2D(Graphics2D g) {
		this.innerGraphics = g;
	}

	@Override
	public void draw(Shape s) {
		innerGraphics.draw(s);
	}

	@Override
	public boolean drawImage(Image img, AffineTransform xform, ImageObserver observer) {
		return innerGraphics.drawImage(img, xform, observer);
	}

	@Override
	public void drawImage(BufferedImage img, BufferedImageOp op, int x, int y) {
		innerGraphics.drawImage(img, op, x, y);
	}

	@Override
	public void drawRenderedImage(RenderedImage img, AffineTransform xform) {
		innerGraphics.drawRenderedImage(img, xform);
	}

	@Override
	public void drawRenderableImage(RenderableImage img, AffineTransform xform) {
		innerGraphics.drawRenderableImage(img, xform);
	}

	@Override
	public void drawString(String str, int x, int y) {
		innerGraphics.drawString(str, x, y);
	}
	
	/**
	 * Renders the given string.
	 * 
	 * @param str The string to render.
	 * @param x The x-coordinate where to render the string.
	 * @param y The y-coordinate where to render the string.
	 */
	public void drawString(String str, double x, double y) {
		innerGraphics.drawString(str, (float) x, (float) y);
	}

	@Override
	public int hashCode() {
		return innerGraphics.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return innerGraphics.equals(obj);
	}
	
	@Override
	public Graphics create(int x, int y, int width, int height) {
		return innerGraphics.create(x, y, width, height);
	}

	@Override
	public FontMetrics getFontMetrics() {
		return innerGraphics.getFontMetrics();
	}

	@Override
	public void drawRect(int x, int y, int width, int height) {
		innerGraphics.drawRect(x, y, width, height);
	}

	public void drawRect(double x, double y, double width, double height) {
		innerGraphics.drawRect((int) x, (int) y, (int) width, (int) height);
	}

	@Override
	public void draw3DRect(int x, int y, int width, int height, boolean raised) {
		innerGraphics.draw3DRect(x, y, width, height, raised);
	}

	@Override
	public void fill3DRect(int x, int y, int width, int height, boolean raised) {
		innerGraphics.fill3DRect(x, y, width, height, raised);
	}

	@Override
	public void drawPolygon(Polygon p) {
		innerGraphics.drawPolygon(p);
	}

	@Override
	public void fillPolygon(Polygon p) {
		innerGraphics.fillPolygon(p);
	}

	@Override
	public void drawChars(char[] data, int offset, int length, int x, int y) {
		innerGraphics.drawChars(data, offset, length, x, y);
	}

	@Override
	public void drawBytes(byte[] data, int offset, int length, int x, int y) {
		innerGraphics.drawBytes(data, offset, length, x, y);
	}

	@Override
	public void finalize() {
		innerGraphics.finalize();
	}

	@Override
	public String toString() {
		return innerGraphics.toString();
	}

	@Override
    @Deprecated
	public Rectangle getClipRect() {
		return innerGraphics.getClipRect();
	}

	@Override
	public boolean hitClip(int x, int y, int width, int height) {
		return innerGraphics.hitClip(x, y, width, height);
	}

	@Override
	public Rectangle getClipBounds(Rectangle r) {
		return innerGraphics.getClipBounds(r);
	}

	@Override
	public Graphics create() {
		return innerGraphics.create();
	}

	@Override
	public Color getColor() {
		return innerGraphics.getColor();
	}

	@Override
	public void setColor(Color c) {
		innerGraphics.setColor(c);
	}

	@Override
	public void setPaintMode() {
		innerGraphics.setPaintMode();
	}

	@Override
	public void setXORMode(Color c1) {
		innerGraphics.setXORMode(c1);
	}

	@Override
	public Font getFont() {
		return innerGraphics.getFont();
	}

	@Override
	public void setFont(Font font) {
		innerGraphics.setFont(font);
	}

	@Override
	public FontMetrics getFontMetrics(Font f) {
		return innerGraphics.getFontMetrics(f);
	}

	@Override
	public Rectangle getClipBounds() {
		return innerGraphics.getClipBounds();
	}

	@Override
	public void clipRect(int x, int y, int width, int height) {
		innerGraphics.clipRect(x, y, width, height);
	}

	@Override
	public void setClip(int x, int y, int width, int height) {
		innerGraphics.setClip(x, y, width, height);
	}

	@Override
	public Shape getClip() {
		return innerGraphics.getClip();
	}

	@Override
	public void setClip(Shape clip) {
		innerGraphics.setClip(clip);
	}

	@Override
	public void copyArea(int x, int y, int width, int height, int dx, int dy) {
		innerGraphics.copyArea(x, y, width, height, dx, dy);
	}

	@Override
	public void drawLine(int x1, int y1, int x2, int y2) {
		innerGraphics.drawLine(x1, y1, x2, y2);
	}

	public void drawLine(double x1, double y1, double x2, double y2) {
		innerGraphics.drawLine((int) x1, (int) y1, (int) x2, (int) y2);
	}
	
	public void drawLine(Point2D fromPoint, Point2D toPoint) {
		drawLine(fromPoint.getX(), fromPoint.getY(), toPoint.getX(), toPoint.getY());
	}

	@Override
	public void fillRect(int x, int y, int width, int height) {
		innerGraphics.fillRect(x, y, width, height);
	}

	@Override
	public void clearRect(int x, int y, int width, int height) {
		innerGraphics.clearRect(x, y, width, height);
	}

	@Override
	public void drawRoundRect(int x, int y, int width, int height, int arcWidth, int arcHeight) {
		innerGraphics.drawRoundRect(x, y, width, height, arcWidth, arcHeight);
	}

	@Override
	public void fillRoundRect(int x, int y, int width, int height, int arcWidth, int arcHeight) {
		innerGraphics.fillRoundRect(x, y, width, height, arcWidth, arcHeight);
	}

	@Override
	public void drawOval(int x, int y, int width, int height) {
		innerGraphics.drawOval(x, y, width, height);
	}

	public void drawOval(double x, double y, double width, double height) {
		innerGraphics.drawOval((int) x, (int) y, (int) width, (int) height);
	}

	public void drawOval(Point2D upperLeftPoint, double width, double height) {
		drawOval(upperLeftPoint.getX(), upperLeftPoint.getY(), width, height);
	}

	public void drawCircle(double x, double y, double r) {
		drawOval(x - r, y - r, 2 * r, 2 * r);
	}

	public void drawCircle(Point2D centerPoint, double radius) {
		drawCircle(centerPoint.getX(), centerPoint.getY(), radius);
	}

	public void drawCircle(Circle c) {
		drawCircle(c.getCenter(), c.getRadius());
	}

	@Override
	public void fillOval(int x, int y, int width, int height) {
		innerGraphics.fillOval(x, y, width, height);
	}

	public void fillOval(double x, double y, double width, double height) {
		innerGraphics.fillOval((int) x, (int) y, (int) width, (int) height);
	}

	public void fillOval(Point2D upperLeftPoint, double width, double height) {
		fillOval(upperLeftPoint.getX(), upperLeftPoint.getY(), width, height);
	}

	public void fillCircle(double x, double y, double r) {
		fillOval(x - r, y - r, 2 * r, 2 * r);
	}

	public void fillCircle(Point2D centerPoint, double radius) {
		fillCircle(centerPoint.getX(), centerPoint.getY(), radius);
	}

	public void fillCircle(Circle c) {
		fillCircle(c.getCenter(), c.getRadius());
	}

	@Override
	public void drawArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
		innerGraphics.drawArc(x, y, width, height, startAngle, arcAngle);
	}

	@Override
	public void fillArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
		innerGraphics.fillArc(x, y, width, height, startAngle, arcAngle);
	}

	@Override
	public void drawString(String str, float x, float y) {
		innerGraphics.drawString(str, x, y);
	}

	@Override
	public void drawPolyline(int[] xPoints, int[] yPoints, int nPoints) {
		innerGraphics.drawPolyline(xPoints, yPoints, nPoints);
	}

	@Override
	public void drawString(AttributedCharacterIterator iterator, int x, int y) {
		innerGraphics.drawString(iterator, x, y);
	}

	@Override
	public void drawPolygon(int[] xPoints, int[] yPoints, int nPoints) {
		innerGraphics.drawPolygon(xPoints, yPoints, nPoints);
	}

	@Override
	public void drawString(AttributedCharacterIterator iterator, float x, float y) {
		innerGraphics.drawString(iterator, x, y);
	}

	@Override
	public void fillPolygon(int[] xPoints, int[] yPoints, int nPoints) {
		innerGraphics.fillPolygon(xPoints, yPoints, nPoints);
	}

	@Override
	public void drawGlyphVector(GlyphVector g, float x, float y) {
		innerGraphics.drawGlyphVector(g, x, y);
	}

	@Override
	public void fill(Shape s) {
		innerGraphics.fill(s);
	}

	@Override
	public boolean hit(Rectangle rect, Shape s, boolean onStroke) {
		return innerGraphics.hit(rect, s, onStroke);
	}

	@Override
	public GraphicsConfiguration getDeviceConfiguration() {
		return innerGraphics.getDeviceConfiguration();
	}

	@Override
	public void setComposite(Composite comp) {
		innerGraphics.setComposite(comp);
	}

	@Override
	public void setPaint(Paint paint) {
		innerGraphics.setPaint(paint);
	}

	@Override
	public boolean drawImage(Image img, int x, int y, ImageObserver observer) {
		return innerGraphics.drawImage(img, x, y, observer);
	}

	/**
	 * Renders the given image.
	 * 
	 * @param str The image to render.
	 * @param x The x-coordinate where to render the image.
	 * @param y The y-coordinate where to render the image.
	 */
	public boolean drawImage(Image img, float x, float y, ImageObserver observer) {
		return innerGraphics.drawImage(img, (int) x, (int) y, observer);
	}

	/**
	 * Renders the given image.
	 * 
	 * @param str The image to render.
	 * @param x The x-coordinate where to render the image.
	 * @param y The y-coordinate where to render the image.
	 */
	public boolean drawImage(Image img, double x, double y, ImageObserver observer) {
		return innerGraphics.drawImage(img, (int) x, (int) y, observer);
	}

	@Override
	public void setStroke(Stroke s) {
		innerGraphics.setStroke(s);
	}

	@Override
	public void setRenderingHint(Key hintKey, Object hintValue) {
		innerGraphics.setRenderingHint(hintKey, hintValue);
	}

	@Override
	public Object getRenderingHint(Key hintKey) {
		return innerGraphics.getRenderingHint(hintKey);
	}

	@Override
	public boolean drawImage(Image img, int x, int y, int width, int height, ImageObserver observer) {
		return innerGraphics.drawImage(img, x, y, width, height, observer);
	}
	
	public boolean drawImage(Image img, double x, double y, double width, double height, ImageObserver observer) {
		return innerGraphics.drawImage(img, (int) x, (int) y, (int) width, (int) height, observer);
	}
	
	public boolean drawImage(Image img, double x, double y, double width, double height) {
		return innerGraphics.drawImage(img, (int) x, (int) y, (int) width, (int) height, null);
	}

	@Override
	public void setRenderingHints(Map<?, ?> hints) {
		innerGraphics.setRenderingHints(hints);
	}

	@Override
	public void addRenderingHints(Map<?, ?> hints) {
		innerGraphics.addRenderingHints(hints);
	}

	@Override
	public RenderingHints getRenderingHints() {
		return innerGraphics.getRenderingHints();
	}

	@Override
	public boolean drawImage(Image img, int x, int y, Color bgcolor, ImageObserver observer) {
		return innerGraphics.drawImage(img, x, y, bgcolor, observer);
	}

	@Override
	public void translate(int x, int y) {
		innerGraphics.translate(x, y);
	}

	@Override
	public void translate(double tx, double ty) {
		innerGraphics.translate(tx, ty);
	}

	@Override
	public void rotate(double theta) {
		innerGraphics.rotate(theta);
	}

	@Override
	public boolean drawImage(Image img, int x, int y, int width, int height, Color bgcolor, ImageObserver observer) {
		return innerGraphics.drawImage(img, x, y, width, height, bgcolor, observer);
	}

	@Override
	public void rotate(double theta, double x, double y) {
		innerGraphics.rotate(theta, x, y);
	}

	@Override
	public void scale(double sx, double sy) {
		innerGraphics.scale(sx, sy);
	}

	@Override
	public void shear(double shx, double shy) {
		innerGraphics.shear(shx, shy);
	}

	@Override
	public boolean drawImage(Image img, int dx1, int dy1, int dx2, int dy2, int sx1, int sy1, int sx2, int sy2,
			ImageObserver observer) {
		return innerGraphics.drawImage(img, dx1, dy1, dx2, dy2, sx1, sy1, sx2, sy2, observer);
	}

	@Override
	public void transform(AffineTransform Tx) {
		innerGraphics.transform(Tx);
	}

	@Override
	public void setTransform(AffineTransform Tx) {
		innerGraphics.setTransform(Tx);
	}

	@Override
	public AffineTransform getTransform() {
		return innerGraphics.getTransform();
	}

	@Override
	public boolean drawImage(Image img, int dx1, int dy1, int dx2, int dy2, int sx1, int sy1, int sx2, int sy2,
			Color bgcolor, ImageObserver observer) {
		return innerGraphics.drawImage(img, dx1, dy1, dx2, dy2, sx1, sy1, sx2, sy2, bgcolor, observer);
	}

	@Override
	public Paint getPaint() {
		return innerGraphics.getPaint();
	}

	@Override
	public Composite getComposite() {
		return innerGraphics.getComposite();
	}

	@Override
	public void setBackground(Color color) {
		innerGraphics.setBackground(color);
	}
	
	public void setBackground(int r, int g, int b) {
		innerGraphics.setBackground(new Color(r, g, b));
	}

	@Override
	public Color getBackground() {
		return innerGraphics.getBackground();
	}

	@Override
	public Stroke getStroke() {
		return innerGraphics.getStroke();
	}

	@Override
	public void clip(Shape s) {
		innerGraphics.clip(s);
	}

	@Override
	public FontRenderContext getFontRenderContext() {
		return innerGraphics.getFontRenderContext();
	}

	@Override
	public void dispose() {
		innerGraphics.dispose();
	}
	
}
