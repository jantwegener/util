package jtw.math;

public interface DFunction {

	public double f(double x);
	
	public double df(double x);
	
}
