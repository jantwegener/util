package jtw.math;

import java.io.Serializable;

import jtw.util.rnd.RND;

public class DMatrix implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final int row;

	private final int col;

	private final double[][] data;

	public DMatrix(int row, int col) {
		data = new double[row][col];

		this.row = row;
		this.col = col;
	}

	public DMatrix(double... d) {
		this(d.length, 1);
		for (int i = 0; i < row; i++) {
			data[i][0] = d[i];
		}
	}

	public void init(double d) {
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				data[i][j] = d;
			}
		}
	}

	public void initRandom() {
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				data[i][j] = RND.nextDouble(-100, 100);
			}
		}
	}
	
	public int size() {
		return row * col;
	}
	
	public int getRow() {
		return row;
	}
	
	public int getCol() {
		return col;
	}

	public void set(int i, int j, int value) {
		data[i][j] = value;
	}

	public double get(int i, int j) {
		return data[i][j];
	}

	public DMatrix transpose() {
		DMatrix tmp = new DMatrix(col, row);

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				tmp.data[j][i] = data[i][j];
			}
		}

		return tmp;
	}

	/**
	 * Expands the matrix by the given number of rows and fills the new rows with
	 * the given value.
	 * 
	 * @param numRow
	 *            The number of rows to expand with.
	 * @param value
	 *            The value to fill the new rows with.
	 * 
	 * @return A new matrix with row+numRow numbers of rows and col columns.
	 */
	public DMatrix expand(int numRow, int value) {
		DMatrix tmp = new DMatrix(row + numRow, col);

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				tmp.data[i][j] = data[i][j];
			}
		}

		for (int i = row; i < row + numRow; i++) {
			for (int j = 0; j < col; j++) {
				tmp.data[i][j] = value;
			}
		}

		return tmp;
	}

	/**
	 * Naive implementation of matrix multiplication.
	 * 
	 * @param m
	 *            The matrix to multiply with.
	 * 
	 * @return A matrix representing the multiplication of this matrix and m.
	 */
	public DMatrix mult(DMatrix m) {
//		DMatrix tmp = new DMatrix(row, m.col);
//
//		for (int i = 0; i < row; i++) {
//			for (int j = 0; j < m.col; j++) {
//				tmp.data[i][j] = 0d;
//				for (int k = 0; k < col; k++) {
//					tmp.data[i][j] += this.data[i][k] * m.data[k][j];
//				}
//			}
//		}
//
//		System.out.println(this + "xx\n" + m + "=\n" + tmp);
//
//		return tmp;
		return mult(m, null);
	}

	/**
	 * Naive implementation of matrix multiplication where to each value the given
	 * function is applied to.
	 * 
	 * @param m
	 *            The matrix to multiply with.
	 * 
	 * @return A matrix representing the multiplication of this matrix and m.
	 */
	public DMatrix mult(DMatrix m, DFunction f) {
		DMatrix tmp = new DMatrix(row, m.col);

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < m.col; j++) {
				tmp.data[i][j] = 0d;
				for (int k = 0; k < col; k++) {
					tmp.data[i][j] += this.data[i][k] * m.data[k][j];
				}
				
				if (f != null) {
					tmp.data[i][j] = f.f(tmp.data[i][j]);
				}
			}
		}

		return tmp;
	}

	public DMatrix add(DMatrix m) {
		DMatrix tmp = new DMatrix(row, m.col);

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < m.col; j++) {
				tmp.data[i][j] = this.data[i][j] + m.data[i][j];
			}
		}

		return tmp;
	}

	public double[][] getData() {
		return data;
	}

	public void setData(double[] d) {
		int k = 0;
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				data[i][j] = d[k++];
			}
		}
	}

	public double[] getDataAsArray() {
		double[] d = new double[row * col];

		int k = 0;
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				d[k++] = data[i][j];
			}
		}

		return d;
	}

	public String toString() {
		StringBuffer b = new StringBuffer();

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				b.append(data[i][j]).append(' ');
			}
			b.append('\n');
		}

		return b.toString();
	}

}
