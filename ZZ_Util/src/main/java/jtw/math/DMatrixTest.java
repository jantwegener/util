package jtw.math;

public class DMatrixTest {

	public static void main(String[] arg) {
		DMatrix m1 = new DMatrix(2, 3);
		DMatrix m2 = new DMatrix(3, 2);

		m1.init(1);
		m2.init(2);

		System.out.println(m1);
		System.out.println(m2);
		System.out.println(m1.mult(m2));

		System.out.println(m2.transpose());
		System.out.println(m1.transpose());

		System.out.println(m1.expand(2, 5));
		System.out.println(m2.expand(5, -1));

		System.out.println(m1.add(m2.transpose()));

		m1.initRandom();
		System.out.println(m1);

		DMatrix m3 = new DMatrix(new double[] { 1, 23, 3 });
		System.out.println(m3);
		System.out.println(m1.mult(m3));
	}

}
