package jtw.math;

public class SigmoidFunction implements DFunction {

	@Override
	public double f(double x) {
		return 1d / (1 + Math.exp(-x));
	}

	@Override
	public double df(double x) {
		return 0;
	}

}
