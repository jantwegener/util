package jtw.neuralnetwork;

import java.io.Serializable;

import jtw.math.DFunction;
import jtw.math.DMatrix;

public class NeuralNetwork implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private DMatrix[] network;
	
	private DFunction f;
	
	int cellSize = 0;
	
	/**
	 * Creates a neural network from the given number of layers.
	 * 
	 * @param layers The number of layers.
	 */
	public NeuralNetwork(int ... layers) {
		this(null, layers);
	}
	
	public NeuralNetwork(DFunction function, int ... layers) {
		f = function;
		network = new DMatrix[layers.length - 1];
		
		for (int i = 1; i < layers.length; i++) {
			network[i - 1] = new DMatrix(layers[i], layers[i - 1] + 1);
			
			cellSize += network[i - 1].size();
		}
	}
	
	public NeuralNetwork(NeuralNetwork nn, double[] weights) {
		f = nn.f;
		network = new DMatrix[nn.network.length];
		cellSize = nn.cellSize;
		
		int index = 0;
		for (int i = 0; i < network.length; i++) {
			double[] w = new double[nn.network[i].size()];
			System.arraycopy(weights, index, w, 0, w.length);
			network[i] = new DMatrix(nn.network[i].getRow(), nn.network[i].getCol());
			network[i].setData(w);
			index += w.length;
		}
	}
	
	public void initRandom() {
		for (DMatrix m : network) {
			m.initRandom();
		}
	}
	
	public double[] feedForward(double ... in) {
		DMatrix input = new DMatrix(in);
		
		for (int i = 0; i < network.length; i++) {
			input = input.expand(1, 1);
			input = network[i].mult(input, f);
		}
		
		return input.getDataAsArray();
	}
	
	public double[] getNetworkAsArray() {
		double[] ret = new double[cellSize];
		int index = 0;
		for (DMatrix matrix : network) {
			double d[] = matrix.getDataAsArray();
			System.arraycopy(d, 0, ret, index, d.length);
			index += d.length;
		}
		return ret;
	}
	
	public String toString() {
		StringBuffer b = new StringBuffer();
		
		for (DMatrix m : network) {
			b.append(m).append('\n');
		}
		
		return b.toString();
	}
	
}
