package jtw.neuralnetwork;

import java.util.Arrays;

import jtw.genetic.Gene;
import jtw.math.SigmoidFunction;

public class NeuralNetworkTest {

	public static void main(String[] arg) {
		NeuralNetwork nn1 = new NeuralNetwork(new SigmoidFunction(), 4, 3, 4, 8, 7);
		nn1.initRandom();
		System.out.println(nn1);

		System.out.println("***");
		
		double[] d = nn1.getNetworkAsArray();
		NeuralNetwork nn2 = new NeuralNetwork(nn1, d);
		System.out.println(nn2);
		
		System.out.println("---");
		
		double[] out = nn1.feedForward(2, 2, 1, 1);
		System.out.println(Arrays.toString(out));
		
		Gene g = new Gene(out);
		out = g.asDoubleArray();
		System.out.println(Arrays.toString(out));
		
		Gene g1 = new Gene(1d, 3d);
		Gene g2 = new Gene(4d, Double.MIN_VALUE);
		g1.crossOver(g2);
		g1.mutate(0.05);
	}
	
}
