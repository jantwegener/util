package jtw.util;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;

public class AntlrUtil {
	
	private AntlrUtil() {
		// avoid instantiation
	}
	
	public static String tokenStreamToString(TokenStream tokenStream, Lexer lexer) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < tokenStream.size(); i++) {
			Token token = tokenStream.get(i);
			builder.append("<").append(token.getText()).append("> : ").append(lexer.getVocabulary().getDisplayName(token.getType())).append("\n");
		}
		return builder.toString();
	}
	
}
