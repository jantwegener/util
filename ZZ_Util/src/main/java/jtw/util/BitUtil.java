package jtw.util;

public class BitUtil {
	
	/**
	 * Translates the bits represented by booleans to an integer.
	 * 
	 * @param bits The bits as booleans. 1 is true, 0 is false.
	 * 
	 * @return The corresponding integer.
	 */
	public static int toInt(boolean ... bits) {
		int retval = 0;
		for (int i = 0; i < bits.length; i++) {
			retval = (retval << 1);
			if (bits[i]) {
				retval |= 1;
			}
		}
		return retval;
	}
	
	/**
	 * Tests whether the bit at the given position is set. For example, the number 5 = b_101 has a bit set at position 0 and 2. For all other positions, the bits are not set.
	 * 
	 * @param value The integer to check.
	 * @param position The position.
	 * 
	 * @return true if the bit is set, false otherwise.
	 */
	public static boolean isBitSet(int value, int position) {
		return ((value >>> position) & 1) == 1;
	}
	
	/**
	 * Flips the bit at the given position.
	 * 
	 * @param value The base integer.
	 * @param position The position.
	 * 
	 * @return The integer with a flipped bit.
	 */
	public static int flipBit(int value, int position) {
		int flipper = 1 << position;
		return value ^ flipper;
	}
	
	/**
	 * Sets the bit at the given position.
	 * 
	 * @param value The base integer.
	 * @param position The position.
	 * 
	 * @return The integer with the bit set.
	 */
	public static int setBit(int value, int position) {
		int flipper = 1 << position;
		return value | flipper;
	}
	
	/**
	 * Clears the bit at the given position.
	 * 
	 * @param value The base integer.
	 * @param position The position.
	 * 
	 * @return The integer with the bit cleared.
	 */
	public static int clearBit(int value, int position) {
		int flipper = ~(1 << position);
		return value & flipper;
	}
	
	/**
	 * Returns only the last 8 bits of the integer. For example, the integer 0xD224 returns 0x0024.
	 * 
	 * @param value The integer to cut the rest of the 8 bits.
	 * 
	 * @return The last 8 bits of the integer.
	 */
	public static int to8Bit(int value) {
		return (value & 0xFF);
	}
	
	/**
	 * Converts the signed byte to an unsigned byte.
	 * 
	 * @param value The signed byte to convert.
	 * 
	 * @return The corresponding unsigned byte.
	 */
	public static int to8Bit(byte value) {
		return (value & 0xFF);
	}
	
	/**
	 * Returns only the last 16 bits of the integer. For example, the integer 0xA9FF_D224 returns 0x0024.
	 * 
	 * @param value The integer to cut the rest of the 16 bits.
	 * 
	 * @return The last 16 bits of the integer. The integer is returned as big-endian.
	 */
	public static int to16Bit(int value) {
		return (value & 0xFFFF);
	}
	
	/**
	 * Returns only the last 16 bits of the integer as little-endian. For example, the integer 0xA9FF_D224 returns 0xD224.
	 * 
	 * @param value The integer to cut the rest of the 16 bits.
	 * 
	 * @return The last 16 bits of the integer. The integer is returned as little-endian.
	 */
	public static int to16BitLE(int value) {
		return ((value & 0xFF00) >>> 8) | ((value & 0xFF) << 8);
	}
	
	/**
	 * Returns only the last 32 bits of the integer as little-endian.
	 * 
	 * @param value The integer to cut the rest of the 32 bits.
	 * 
	 * @return The last 32 bits of the integer. The integer is returned as little-endian.
	 */
	public static int to32BitLE(int value) {
		return ((value & 0xFF) << 24) | ((value & 0xFF00) << 8) | ((value & 0xFF0000) >>> 8) | ((value & 0xFF000000) >>> 24);
	}
	
	/**
	 * Returns a 16 bit integer created from the least-significant bits and most-significant bits.
	 * 
	 * @param lsb The least significant bits.
	 * @param msb The most significant bits.
	 * 
	 * @return The integer as big-endian.
	 */
	public static int to16Bit(int msb, int lsb) {
		return (lsb & 0xFF) | ((msb & 0xFF) << 8);
	}
	
	/**
	 * Returns a 16 bit integer created from the least-significant bits and most-significant bits.
	 * 
	 * @param lsb The least significant bits.
	 * @param msb The most significant bits.
	 * 
	 * @return The integer as little-endian.
	 */
	public static int to16BitLE(int msb, int lsb) {
		return (msb & 0xFF) | ((lsb & 0xFF) << 8);
	}
	
}
