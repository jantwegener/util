package jtw.util;

public class BooleanUtil {
	
	/**
	 * Returns <code>true</code> if and only if the string representation of the object is true.
	 * This can be either the string "true" (case insensitive) or "1".
	 * 
	 * @param obj
	 *            The object to test.
	 * 
	 * @return true if the string value of the object corresponds to true.
	 * 
	 * @see Boolean#parseBoolean(String)
	 */
	public static boolean isTrue(Object obj) {
		if (obj instanceof Boolean) {
			return ((Boolean) obj).booleanValue();
		}
		
		// String.valueOf(..) never returns null
		String str = String.valueOf(obj);
		if ("1".equals(str)) {
			str = "true";
		}
		return "true".equalsIgnoreCase(str);
	}
	
	/**
	 * Returns true if the object does not represent true. It returns the negation of {@link #isTrue(Object)}.
	 * 
	 * @param obj The object to test.
	 * 
	 * @return true if the string value of the object does not corresponds to true.
	 */
	public static boolean isNotTrue(Object obj) {
		return !isTrue(obj);
	}
	
	/**
	 * Returns <code>true</code> if and only if the string representation of the object is false.
	 * This can be either the string "false" (case insensitive) or "0".
	 * 
	 * @param obj
	 *            The object to test.
	 * 
	 * @return true if the string value of the object corresponds to false.
	 * 
	 * @see Boolean#parseBoolean(String)
	 */
	public static boolean isFalse(Object obj) {
		if (obj instanceof Boolean) {
			return ((Boolean) obj).booleanValue();
		}
		
		// String.valueOf(..) never returns null
		String str = String.valueOf(obj);
		if ("0".equals(str)) {
			str = "false";
		}
		return "false".equalsIgnoreCase(str);
	}
	
	/**
	 * Converts the boolean to a string.
	 * 
	 * @param b The boolean to check.
	 * @param trueStr The string given that the boolean is <code>true</code>.
	 * @param falseStr The string given that the boolean is <code>false</code>.
	 * 
	 * @return A string representing the boolean.
	 */
	public static String toString(boolean b, String trueStr, String falseStr) {
		if (b) {
			return trueStr;
		}
		return falseStr;
	}
	
	/**
	 * Converts the boolean object to a string.
	 * 
	 * @param b The object to check.
	 * @param trueStr The string given that the boolean is <code>true</code>.
	 * @param falseStr The string given that the boolean is not <code>true</code>.
	 * 
	 * @return A string representing the boolean.
	 * 
	 * @see #isTrue(Object)
	 */
	public static String toString(Object b, String trueStr, String falseStr) {
		if (isTrue(b)) {
			return trueStr;
		}
		return falseStr;
	}
	
}
