package jtw.util;

import java.util.logging.Logger;

public class Converter {

	private final static Logger LOG = Logger.getLogger(Converter.class.getName());

	public static Long toLong(String s) {
		Long ll = null;
		try {
			if (s != null) {
				ll = Long.valueOf(s);
			}
		} catch (NumberFormatException nfe) {
			// simply ignore
			LOG.fine("" + nfe.getStackTrace());
		}
		return ll;
	}

	public static Integer toInteger(String s) {
		Integer ii = null;
		try {
			if (s != null) {
				ii = Integer.valueOf(s);
			}
		} catch (NumberFormatException nfe) {
			// simply ignore
			LOG.fine("" + nfe.getStackTrace());
		}
		return ii;
	}

}
