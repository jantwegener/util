package jtw.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	
	/**
	 * Converts the string to a {@link Calendar} object. The string must be of
	 * the form "dd.MM.yyyy".
	 * 
	 * @param date The date as a string to convert.
	 * 
	 * @return The converted {@link Calendar} object.
	 */
	public static Calendar toCalendar(String date) throws ParseException {
		return toCalendar(date, "dd.MM.yyyy");
	}
	
	public static Calendar toCalendar(String date, String format) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat(format);
		Date d = df.parse(date);
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		return c;
	}

	public static Calendar toCalendar(String date, Calendar defaultDate) {
		return toCalendar(date, "dd.MM.yyyy", defaultDate);
	}
	
	public static Calendar toCalendar(String date, String format, Calendar defaultDate) {
		SimpleDateFormat df = new SimpleDateFormat(format);
		Calendar retVal;
		
		try {
			Date d = df.parse(date);
			Calendar c = Calendar.getInstance();
			c.setTime(d);
			retVal = c;
		} catch (ParseException e) {
			retVal = defaultDate;
		}
		return retVal;
	}
	
	/**
	 * Converts a string to a {@link Calendar} object. When parsing the parameter {@code date} fails,
	 * the parameter {@code defaultDate} is tried. However, when this also fails, a {@link RuntimeException} is thrown.
	 * 
	 * @param date The date as a string to convert.
	 * @param format The format of the date.
	 * @param defaultDate A default string.
	 * 
	 * @return The converted {@link Calendar} object.
	 */
	public static Calendar toCalendar(String date, String format, String defaultDate) {
		Calendar retVal;
	
		try {
			retVal = toCalendar(date, format);
		} catch (ParseException e) {
			try {
				retVal = toCalendar(defaultDate, format);
			} catch (ParseException e1) {
				throw new RuntimeException(e1);
			}
		}
		return retVal;
	}
	
	public static Calendar toCalendar(double ms) {
		Date d = new Date((long) ms);
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		return c;
	}
	
	public static LocalDate toLocalDate(String date, String format) throws DateTimeParseException {
		return LocalDate.parse(date, DateTimeFormatter.ofPattern(format));
	}
	
	public static LocalDate toLocalDate(String date, DateTimeFormatter formatter) throws DateTimeParseException {
		return LocalDate.parse(date, formatter);
	}

	public static String toString(Calendar c) {
		return toString(c, "dd.MM.yyyy");
	}
	
	public static String toString(Calendar c, String format) {
		SimpleDateFormat df = new SimpleDateFormat(format);
		return df.format(c.getTime());
	}
	
	public static String toString(LocalDate date, DateTimeFormatter formatter) {
		return date.format(formatter);
	}
	
	public static String toString(LocalDate date, String format) {
		return date.format(DateTimeFormatter.ofPattern(format));
	}
	
}
