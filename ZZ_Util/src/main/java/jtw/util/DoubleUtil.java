package jtw.util;

import java.util.Comparator;

public class DoubleUtil {
	
	/**
	 * Returns the parsed double with a default value of 0.
	 * 
	 * @param d The string to parse.
	 * 
	 * @return The parsed double or 0 if the string cannot be parsed.
	 */
	public static double toDouble(String d) {
		return toDouble(d, 0d);
	}
	
	/**
	 * Returns the parsed double of the given default value if the parsing fails.
	 * 
	 * @param d The string to parse.
	 * @param defVal The default value if the parsing fails.
	 * 
	 * @return The parsed double or the default value if the string cannot be parsed.
	 */
	public static double toDouble(String d, double defVal) {
		double ret = defVal;
		if (d != null) {
			d = d.replace(',', '.');
			try {
				ret = Double.parseDouble(d);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}
	
	/**
	 * Returns the double from the given string. The string can represent a fraction of the form x/y.
	 * 
	 * @param d The string representing a fraction of the form x or x/y.
	 * @param defVal The default value if the parsing fails.
	 * 
	 * @return The parsed double or the default value if the string cannot be parsed.
	 */
	public static double fractionToDouble(String d, double defVal) {
		double ret = defVal;
		if (d != null) {
			d = d.replace(',', '.');
			String[] fraction = d.split("/", 2);
			try {
				if (fraction.length >= 2) {
					double d1 = Double.parseDouble(fraction[0]);
					double d2 = Double.parseDouble(fraction[1]);
					ret = d1 / d2;
				} else {
					ret = Double.parseDouble(d);
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}
	
	public static long toLong(String d, int mult) {
		long ret = 0l;
		if (d != null) {
			if (!"-".equals(d)) {
				try {
					double x = toDouble(d) * mult;
					ret = (long) x;
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
		}
		return ret;
	}
	
	/**
	 * Checks if the two doubles are equal within an error range of epsilon.
	 * 
	 * @param d1 A double.
	 * @param d2 A double.
	 * @param epsilon The error range.
	 * 
	 * @return true if the two given doubles are equal (within the error range), false otherwise.
	 */
	public static boolean equals(double d1, double d2, double epsilon) {
		return Math.abs(d1 - d2) < epsilon;
	}
	
	/**
	 * Compares the two doubles within an error range of epsilon.
	 * 
	 * @param d1 A double.
	 * @param d2 A double.
	 * @param epsilon The error range.
	 * 
	 * @return 0 if the two given doubles are equal (within the error range), -1 if the first double is less than the second and 1 if the first double is greater than the second.
	 * 
	 * @see Comparator#compare(Object, Object)
	 */
	public static int compare(double d1, double d2, double epsilon) {
		if (equals(d1, d2, epsilon)) {
			return 0;
		}
		if (d1 < d2) {
			return -1;
		}
		
		return 1;
	}
	
}
