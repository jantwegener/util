package jtw.util;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

/**
 * A util class for images.
 * 
 * Converts between a 2d and a 1d coordinate system. This occurs for example when painting on the screen.
 * 
 * @author Jan-Thierry Wegener
 */
public final class ImageUtil {
	
	/**
	 * The constant PI / 2.
	 */
	public static final double PI_HALF = Math.PI * 0.5d;
	
	private ImageUtil() {
		// prevent instantiation
	}
	
	public static BufferedImage createToBufferedImage(Image img) {
		if (img instanceof BufferedImage) {
			return (BufferedImage) img;
		}
		
		// create our buffered image by hand
		BufferedImage bufferedImage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics = bufferedImage.createGraphics();
		graphics.drawImage(img, 0, 0, null);
		graphics.dispose();
		
		return bufferedImage;
	}
	
	/**
	 * Returns a rotated copy of the image (clock wise). The rotation is by 90 degrees.
	 * 
	 * @param img The image to rotate.
	 * 
	 * @return A rotated copy of the image.
	 */
	public static BufferedImage rotateClockWise(BufferedImage img) {
		return rotate(img, 1, PI_HALF);
	}
	
	/**
	 * Returns a rotated copy of the image (clock wise). The rotation is by 90 degrees, and that <code>num</code> times.
	 * 
	 * @param img The image to rotate.
	 * @param num How often the image is rotated by 90 degrees.
	 * 
	 * @return A rotated copy of the image.
	 */
	public static BufferedImage rotateClockWise(BufferedImage img, int num) {
		return rotate(img, num, PI_HALF);
	}

	/**
	 * Returns a rotated copy of the image (counter clock wise). The rotation is by 90 degrees.
	 * 
	 * @param img The image to rotate.
	 * 
	 * @return A rotated copy of the image.
	 */
	public static BufferedImage rotateCounterClockWise(BufferedImage img) {
		return rotate(img, 1, -PI_HALF);
	}

	/**
	 * Returns a rotated copy of the image (counter clock wise). The rotation is by 90 degrees, and that <code>num</code> times.
	 * 
	 * @param img The image to rotate.
	 * @param num How often the image is rotated by 90 degrees.
	 * 
	 * @return A rotated copy of the image.
	 */
	public static BufferedImage rotateCounterClockWise(BufferedImage img, int num) {
		return rotate(img, num, -PI_HALF);
	}
	
	/**
	 * Does the hard work of the rotation. Only works for 90 degrees rotations!
	 * 
	 * @param img The image to rotate.
	 * @param num The number of times to rotate.
	 * @param rads The rotation in radiants.
	 * 
	 * @return A rotated copy of the image.
	 */
	private static BufferedImage rotate(BufferedImage img, int num, double rads) {
		final int w;
		final int h;
		if ((num % 2) == 0) {
			// width and height are kept
			w = img.getWidth();
			h = img.getHeight();
		} else {
			// width and height are swapped
			w = img.getHeight();
			h = img.getWidth();
		}
		final AffineTransform at = new AffineTransform();
		at.translate(w / 2, h / 2);
		at.rotate(num * rads, 0, 0);
		at.translate(-img.getWidth() / 2, -img.getHeight() / 2);
		final AffineTransformOp rotateOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		final BufferedImage rotatedImage = new BufferedImage(w, h, img.getType());
		rotateOp.filter(img, rotatedImage);
		return rotatedImage;
	}
	
	public static BufferedImage rotate(BufferedImage img, double rads) {
		double sin = Math.abs(Math.sin(rads));
		double cos = Math.abs(Math.cos(rads));
		int w = (int) Math.floor(img.getWidth() * cos + img.getHeight() * sin);
		int h = (int) Math.floor(img.getHeight() * cos + img.getWidth() * sin);
		BufferedImage rotatedImage = new BufferedImage(w, h, img.getType());
		AffineTransform at = new AffineTransform();
		at.translate(w / 2, h / 2);
		at.rotate(rads,0, 0);
		at.translate(-img.getWidth() / 2, -img.getHeight() / 2);
		AffineTransformOp rotateOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		rotateOp.filter(img, rotatedImage);
		return rotatedImage;
	}
    
    /**
     * Converts the given coordinates to an index in a one-dimensional array.
     * 
     * @param x
     *            The x-coordinate.
     * @param y
     *            The y-coordinate.
     * @param width
     *            The width of the canvas or image.
     * 
     * @return The index in a one-dimensional array.
     */
    public static int getIndex(double x, double y, int width) {
        return getIndex((int) x, (int) y, width);
    }
    
    /**
     * Converts the given coordinates to an index in a one-dimensional array.
     * 
     * @param x
     *            The x-coordinate.
     * @param y
     *            The y-coordinate.
     * @param width
     *            The width of the canvas or image.
     * 
     * @return The index in a one-dimensional array.
     */
	public static int getIndex(int x, int y, int width) {
		int index = y * width + x;
		return index;
	}

    /**
     * Converts the given coordinates to an index in a one-dimensional array.
     * 
     * @param p
     *            The point containing the x-coordinate and the y-coordinate.
     * @param width
     *            The width of the canvas or image.
     * 
     * @return The index in a one-dimensional array.
     */
	public static int getIndex(Point p, int width) {
		return getIndex(p.x, p.y, width);
	}
	
    /**
     * Converts a given index of a one-dimensional array to coordinates.
     * 
     * @param index
     *            The index of a one-dimensional array.
     * @param width
     *            The width of the canvas or image.
     * 
     * @return The coordinates corresponding to the index.
     */
	public static int[] getCoordinates(int index, int width) {
		int x = index % width;
		int y = index / width;
		return new int[] { x, y };
	}
	
    /**
     * Converts a given index of a one-dimensional array to a point.
     * 
     * @param index
     *            The index of a one-dimensional array.
     * @param width
     *            The width of the canvas or image.
     * 
     * @return The point corresponding to the index.
     */
	public static Point getPoint(int index, int width) {
		int x = index % width;
		int y = index / width;
		return new Point(x, y);
	}
	
    /**
     * Returns the neighbors of the given point. The width and height have to be understood as the radius, meaning that
     * a width of 3 and height of 1, actually returns 7 * 3 = 21 values. Whether the given point is returned, is
     * controlled by the boolean returnItself.
     * 
     * @param data
     *            The data to return the neighbors from.
     * @param w
     *            The width of a row in the data.
     * @param h
     *            The height of a column in the data.
     * @param x
     *            The x-coordinate in the data.
     * @param y
     *            The y-coordinate in the data.
     * @param width
     *            The width of the neighbor area.
     * @param height
     *            The height of the neighbor area.
     * @param returnItself
     *            <code>true</code> if the given point shall also be returned, <code>false</code> otherwise.
     * 
     * @return The neighbors of the given point.
     */
    public static int[] getNeighbors(int[] data, int w, int h, int x, int y, int width, int height, boolean returnItself) {
        int size = returnItself ? ((2 * width) + 1) * ((2 * width) + 1) : (((2 * width) + 1) * ((2 * width) + 1)) - 1;
        int[] neighbor = new int[size];
        
        int p = 0;
        for (int i = -width; i < width; i++) {
            for (int j = -height; j < height; j++) {
                int xx = x + i;
                int yy = y + j;
                if ((xx >= 0) && (xx < w) && (yy >= 0) && (yy < h)) {
                    if (!returnItself || (i != 0) || (j != 0)) {
                        neighbor[p++] = data[getIndex(xx, yy, w)];
                    }
                }
            }
        }
        
        int[] retval;
        if (p == size) {
            retval = neighbor;
        } else {
            retval = new int[p];
            System.arraycopy(neighbor, 0, retval, 0, p);
        }
        
        return retval;
    }
    
}
