package jtw.util;

import java.util.logging.Level;
import java.util.logging.Logger;

public class IntegerUtil {

	private final static Logger LOG = Logger.getLogger(IntegerUtil.class.getName());

	/**
	 * Returns an integer representation of the given value or the default value if no integer is given.
	 * 
	 * @param i The string to convert to an integer.
	 * @param defVal The value to use if no integer is given.
	 * 
	 * @return An integer.
	 */
	public static int toInt(String i, int defVal) {
		int retVal = defVal;
		try {
			retVal = Integer.parseInt(i.trim());
		} catch (NumberFormatException | NullPointerException nfe) {
			if (LOG.isLoggable(Level.FINE)) {
				LOG.fine("" + nfe.getStackTrace());
			}
		}
		return retVal;
	}
	
	public static int toInt(Object i, int defVal) {
		int retVal = defVal;
		if (i != null) {
			retVal = toInt(i.toString(), defVal);
		}
		return retVal;
	}
	
}
