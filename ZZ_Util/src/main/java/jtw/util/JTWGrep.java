package jtw.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class JTWGrep {
    
    public static void main(String[] args) throws Exception {
        if ((args.length < 2) || args[0].equals("-h")) {
            System.out.printf("Usage: java %s REGEX FOLDER\n", JTWGrep.class.getName());
            System.exit(1);
        }
        
        String regex = args[0];
        String infolder = args[1];
        int[] found = new int[1];
        
        search(infolder, regex, found);
        
        System.out.println("We found " + found[0] + " matches.");
    }
    
    private static boolean ignore(File file) {
        boolean retval = false;
        if (file.getAbsolutePath().contains(".svn")) {
            retval = true;
        }
        return retval;
    }
    
    private static void search(String file, String regex, int[] found) throws IOException {
        File folder = new File(file);
        
        if (folder.isDirectory()) {
            for (File f : folder.listFiles()) {
                if (!ignore(f)) {
                    search(f.getAbsolutePath(), regex, found);
                }
            }
        } else {
            grepFile(folder.getAbsolutePath(), regex, found);
        }
    }
    
    private static void grepFile(String file, String regex, int[] found) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            int lineNumber = 0;
            String line = null;
            while ((line = reader.readLine()) != null) {
                line = line.trim();
                lineNumber++;
                
                if (line.matches(".*" + regex + ".*")) {
                    System.out.printf("%s:%s>> %s\n", file, lineNumber, line);
                    found[0]++;
                }
            }
        }
    }
    
}
