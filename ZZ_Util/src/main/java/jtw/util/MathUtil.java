package jtw.util;

import static java.lang.Math.PI;
import static java.lang.Math.sin;

import java.util.List;

public class MathUtil {
	
	/**
	 * Computes the sinc of x. The function sinc is defined as sin(pi x) / (pi x).
	 * 
	 * @param x The value x.
	 * 
	 * @return The value of sinc(x).
	 */
	public static double sinc(double x) {
		if (x == 0) {
			return 1d;
		}
		return sin(PI * x) / PI / x;
	}
	
	/**
	 * Returns the maximum value of the given ones.
	 * 
	 * @param x The array to check.
	 * 
	 * @return The maximum.
	 */
	public static double max(double ... x) {
		double retval = Double.NEGATIVE_INFINITY;
		for (double xx : x) {
			if (xx > retval) {
				retval = xx;
			}
		}
		
		return retval;
	}
	
	/**
	 * Returns the maximum value of the given ones.
	 * 
	 * @param x The array to check.
	 * 
	 * @return The maximum.
	 */
	public static int max(int ... x) {
		int retval = Integer.MIN_VALUE;
		for (int xx : x) {
			if (xx > retval) {
				retval = xx;
			}
		}
		
		return retval;
	}

	/**
	 * Returns the minimum value of the given ones.
	 * 
	 * @param x The array to check.
	 * 
	 * @return The minimum.
	 */
	public static double min(double ... x) {
		double retval = Double.POSITIVE_INFINITY;
		for (double xx : x) {
			if (xx < retval) {
				retval = xx;
			}
		}
		
		return retval;
	}

	/**
	 * Returns the minimum value of the given ones.
	 * 
	 * @param x The array to check.
	 * 
	 * @return The minimum.
	 */
	public static int min(int ... x) {
		int retval = Integer.MAX_VALUE;
		for (int xx : x) {
			if (xx < retval) {
				retval = xx;
			}
		}
		
		return retval;
	}
    
    /**
     * Constraints the value by the given interval. Always returns a value in the interval [min, max].
     * 
     * @param x
     *            The value to contraint.
     * @param min
     *            The minimal value.
     * @param max
     *            The maximal value.
     * 
     * @return The original value x if the value is in [min, max], the value min if x < min and the value max if x >
     *         max.
     */
    public static double constraint(double x, double min, double max) {
        double retval = x;
        if (x < min) {
            retval = min;
        } else if (x > max) {
            retval = max;
        }
        
        return retval;
    }
    
    /**
     * Remaps a value from an interval to a target interval.
     * 
     * @param x
     *            The value to map. Must be in the interval [origMin, origMax].
     * @param origMin
     *            The original interval minimum.
     * @param origMax
     *            The original interval maximum.
     * @param targetMin
     *            The target interval minimum.
     * @param targetMax
     *            The target interval maximum.
     * 
     * @return A remapped value in the interval [targetMin, targetMax].
     */
    public static double remap(double x, double origMin, double origMax, double targetMin, double targetMax) {
        return targetMin + (((x - origMin) * (targetMax - targetMin)) / (origMax - origMin));
    }
    
    /**
	 * Computes the average of the given list within the range of the given start
	 * and stop index. When the start or stop index are out of bounds, the
	 * computation may take less values into account. The computations then are
	 * based on modified start and stop indexes.
	 * <p>
	 * The list must provide a fast access to {@link List#get(int)}.
	 * 
	 * @param objectList
	 *            The list of objects to calculate the average from.
	 * @param otd
	 *            The interface to get the double value from the objects in the list.
	 * @param startIndex
	 *            The index from where to start the calculation. Inclusive.
	 * @param stopIndex
	 *            The index where to stop the calculation. Exclusive.
	 * 
	 * @return The average of the values.
	 */
    public static<E> double average(List<? extends E> objectList, ObjectToDouble<E> otd, int startIndex, int stopIndex) {
    	if (startIndex < 0) {
    		startIndex = 0;
    	}
    	if (stopIndex > objectList.size()) {
    		stopIndex = objectList.size();
    	}

    	double avg = 0;
    	int n = stopIndex - startIndex;
    	if (n > 0) {
	    	for (int i = startIndex; i < stopIndex; i++) {
	    		avg += otd.objToDouble(objectList.get(i));
	    	}
	    	avg /= n;
    	}
    	
    	return avg;
    }

    /**
	 * Computes the average of the given list within the range of the given start
	 * and stop index. When the start or stop index are out of bounds, the
	 * computation may take less values into account. The computations then are
	 * based on modified start and stop indexes.
	 * <p>
	 * The list must provide a fast access to {@link List#get(int)}.
	 * 
	 * @param objectArray
	 *            The array of objects to calculate the average from.
	 * @param otd
	 *            The interface to get the double value from the objects in the list.
	 * @param startIndex
	 *            The index from where to start the calculation. Inclusive.
	 * @param stopIndex
	 *            The index where to stop the calculation. Exclusive.
	 * 
	 * @return The average of the values.
	 */
    public static<E> double average(E[] objectArray, ObjectToDouble<E> otd, int startIndex, int stopIndex) {
    	if (startIndex < 0) {
    		startIndex = 0;
    	}
    	if (stopIndex > objectArray.length) {
    		stopIndex = objectArray.length;
    	}

    	double avg = 0;
    	int n = stopIndex - startIndex;
    	if (n > 0) {
	    	for (int i = startIndex; i < stopIndex; i++) {
	    		avg += otd.objToDouble(objectArray[i]);
	    	}
	    	avg /= n;
    	}
    	
    	return avg;
    }

    public static double average(double[] objectArray, ObjectToDouble<Double> otd, int startIndex, int stopIndex) {
    	if (startIndex < 0) {
    		startIndex = 0;
    	}
    	if (stopIndex > objectArray.length) {
    		stopIndex = objectArray.length;
    	}

    	double avg = 0;
    	int n = stopIndex - startIndex;
    	if (n > 0) {
	    	for (int i = startIndex; i < stopIndex; i++) {
	    		avg += otd.objToDouble(objectArray[i]);
	    	}
	    	avg /= n;
    	}
    	
    	return avg;
    }
    
    /**
     * Computes the mean absolute deviation around the given center point.
     * 
     * @param objectArray The array of objects to calculate the average from.
     * @param otd The interface to get the double value from the objects in the list.
     * @param centerValue The center point to compute the value around.
     * @param startIndex The index from where to start the calculation. Inclusive.
     * @param stopIndex The index where to stop the calculation. Exclusive.
     * 
     * @return The mean absolute deviation around the given center.
     */
    public static<E> double meanAbsoluteDeviation(E[] objectArray, ObjectToDouble<E> otd, double centerValue, int startIndex, int stopIndex) {
    	if (startIndex < 0) {
    		startIndex = 0;
    	}
    	if (stopIndex > objectArray.length) {
    		stopIndex = objectArray.length;
    	}
    	
    	double avg = 0;
    	int n = stopIndex - startIndex;
    	if (n > 0) {
	    	for (int i = startIndex; i < stopIndex; i++) {
	    		avg += Math.abs(otd.objToDouble(objectArray[i]) - centerValue);
	    	}
	    	avg /= n;
    	}
    	
    	return avg;
    }
    
    public static<E> double meanAbsoluteDeviation(List<E> objectArray, ObjectToDouble<E> otd, double centerValue, int startIndex, int stopIndex) {
    	if (startIndex < 0) {
    		startIndex = 0;
    	}
    	if (stopIndex > objectArray.size()) {
    		stopIndex = objectArray.size();
    	}
    	
    	double avg = 0;
    	int n = stopIndex - startIndex;
    	if (n > 0) {
	    	for (int i = startIndex; i < stopIndex; i++) {
	    		avg += Math.abs(otd.objToDouble(objectArray.get(i)) - centerValue);
	    	}
	    	avg /= n;
    	}
    	
    	return avg;
    }

    public static<E> double meanAbsoluteDeviation(double[] objectArray, ObjectToDouble<Double> otd, double centerValue, int startIndex, int stopIndex) {
    	if (startIndex < 0) {
    		startIndex = 0;
    	}
    	if (stopIndex > objectArray.length) {
    		stopIndex = objectArray.length;
    	}
    	
    	double avg = 0;
    	int n = stopIndex - startIndex;
    	if (n > 0) {
	    	for (int i = startIndex; i < stopIndex; i++) {
	    		avg += Math.abs(otd.objToDouble(objectArray[i]) - centerValue);
	    	}
	    	avg /= n;
    	}
    	
    	return avg;
    }

    /**
     * Computes the standard deviation around the given center point.
     * 
     * @param objectArray The array of objects to calculate the average from.
     * @param otd The interface to get the double value from the objects in the list.
     * @param centerValue The center point to compute the value around.
     * @param startIndex The index from where to start the calculation. Inclusive.
     * @param stopIndex The index where to stop the calculation. Exclusive.
     * 
     * @return The standard deviation around the given center.
     */
    public static<E> double standardDeviation(E[] objectArray, ObjectToDouble<E> otd, double centerValue, int startIndex, int stopIndex) {
    	if (startIndex < 0) {
    		startIndex = 0;
    	}
    	if (stopIndex > objectArray.length) {
    		stopIndex = objectArray.length;
    	}
    	
    	double avg = 0;
    	int n = stopIndex - startIndex;
    	if (n > 0) {
	    	for (int i = startIndex; i < stopIndex; i++) {
	    		double val = otd.objToDouble(objectArray[i]) - centerValue;
	    		avg += val * val;
	    	}
	    	avg /= (n - 1);
    	}
    	avg = Math.sqrt(avg);
    	
    	return avg;
    }
    
    public static<E> double standardDeviation(List<E> objectArray, ObjectToDouble<E> otd, double centerValue, int startIndex, int stopIndex) {
    	if (startIndex < 0) {
    		startIndex = 0;
    	}
    	if (stopIndex > objectArray.size()) {
    		stopIndex = objectArray.size();
    	}
    	
    	double avg = 0;
    	int n = stopIndex - startIndex;
    	if (n > 0) {
	    	for (int i = startIndex; i < stopIndex; i++) {
	    		double val = otd.objToDouble(objectArray.get(i)) - centerValue;
	    		avg += val * val;
	    	}
	    	avg /= (n - 1);
    	}
    	avg = Math.sqrt(avg);
    	
    	return avg;
    }

    public static<E> double standardDeviation(double[] objectArray, ObjectToDouble<Double> otd, double centerValue, int startIndex, int stopIndex) {
    	if (startIndex < 0) {
    		startIndex = 0;
    	}
    	if (stopIndex > objectArray.length) {
    		stopIndex = objectArray.length;
    	}
    	
    	double avg = 0;
    	int n = stopIndex - startIndex;
    	if (n > 0) {
	    	for (int i = startIndex; i < stopIndex; i++) {
	    		double val = Math.abs(otd.objToDouble(objectArray[i]) - centerValue);
	    		avg += val * val;
	    	}
	    	avg /= (n - 1);
    	}
    	avg = Math.sqrt(avg);
    	
    	return avg;
    }
    
    public static<E> double[] simpleLinearRegression(List<E> objectXArray, List<E> objectYArray, ObjectToDouble<E> getX, ObjectToDouble<E> getY, int startIndex, int stopIndex) {
    	double alpha = 0;
    	double beta = 0;
    	
    	double x = average(objectXArray, getX, startIndex, stopIndex);
    	double y = average(objectYArray, getY, startIndex, stopIndex);
    	
    	double cov = 0;
    	double var = 0;
    	for (int i = startIndex; i < stopIndex; i++) {
    		E currX = objectXArray.get(i);
    		E currY = objectYArray.get(i);
    		double xc = getX.objToDouble(currX);
    		double yc = getY.objToDouble(currY);
    		cov += (xc - x) * (yc - y);
    		var += (xc - x) * (xc - x);
    	}
    	beta = cov / var;
    	
    	alpha = y - beta * x;
    	
    	return new double[] {alpha, beta};
    }
    
    /**
     * Computes the distance to the power of two between the two points.
     * 
     * @param x One point.
     * @param y Another point.
     * 
     * @return The distance between the two points.
     */
    public static float distancePow2(float[] x, float[] y) {
    	float retval = 0;
    	for (int i = 0; i < x.length; i++) {
    		retval += (x[i] - y[i]) * (x[i] - y[i]);
    	}
    	return retval;
    }
    
    /**
     * Computes the distance to the power of two between the two points.
     * 
     * @param x One point.
     * @param y Another point.
     * 
     * @return The distance between the two points.
     */
    public static float distanceManhattanPow2(float[] x, float[] y) {
    	float retval = 0;
    	for (int i = 0; i < x.length; i++) {
    		retval += Math.abs(x[i] - y[i]);
    	}
    	return retval;
    }
    
}
