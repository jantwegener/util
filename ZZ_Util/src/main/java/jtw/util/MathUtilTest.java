package jtw.util;

public class MathUtilTest {
    
    public static void main(String[] args) {
        double x = MathUtil.constraint(5, 4, 8);
        System.out.println(x);
        x = MathUtil.constraint(7, 4, 8);
        System.out.println(x);
        x = MathUtil.constraint(2, 4, 8);
        System.out.println(x);
        x = MathUtil.constraint(15, 4, 8);
        System.out.println(x);
        
        System.out.println("----");
        
        x = MathUtil.remap(1, 0, 1, 4, 8);
        System.out.println(x);
        x = MathUtil.remap(0.5, 0, 1, 4, 8);
        System.out.println(x);
        x = MathUtil.remap(1, 1, 2, 0, 2);
        System.out.println(x);
        
        x = MathUtil.remap(1, 1, 2, 1, 3);
        System.out.println(x);
        x = MathUtil.remap(2, 1, 4, 1, 7);
        System.out.println(x);
        x = MathUtil.remap(2, 1, 4, -1, 7);
        System.out.println(x);
        x = MathUtil.remap(1, 1, 4, -1, 7);
        System.out.println(x);
        x = MathUtil.remap(2, 1, 4, -1, -7);
        System.out.println(x);
        x = MathUtil.remap(4, 1, 4, 8, 2);
        System.out.println(x);
    }
    
}
