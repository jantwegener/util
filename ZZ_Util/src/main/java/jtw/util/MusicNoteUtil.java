package jtw.util;

import jtw.util.exception.UnknownAbcNoteException;

public final class MusicNoteUtil {
	
	private MusicNoteUtil() {
		// avoid instantiation
	}
	
	/**
	 * Returns the frequency of the note with its modifiers (in ABC notation).
	 * 
	 * @param abcTune The tune of the note. Can be c, d, e, f, g, a, b or C, D, E, F, G, A, B.
	 * @param abcModifier The modifiers of the note. Can be "'" or ",".
	 * 
	 * @return The frequency of the note.
	 */
	public static double getPianoFrequency(String abcTune, String abcModifier) {
		double power = (getPianoKey(abcTune, abcModifier) - 49) / 12d;
		double retval = Math.pow(2, power) * 440;
		return retval;
	}
	
	/**
	 * Returns the corresponding piano key of the note with its modifiers (in ABC notation).
	 * 
	 * @param abcTune The tune of the note. Can be c, d, e, f, g, a, b or C, D, E, F, G, A, B.
	 * @param abcModifier The modifiers of the note. Can be "'" or ",".
	 * 
	 * @return The piano key of the note.
	 */
	public static int getPianoKey(String abcTune, String abcModifier) {
		int retval;
		
		switch (abcTune) {
			// major
			case "C":
				retval = 40;
				break;
			case "D":
				retval = 42;
				break;
			case "E":
				retval = 44;
				break;
			case "F":
				retval = 45;
				break;
			case "G":
				retval = 47;
				break;
			case "A":
				retval = 49;
				break;
			case "B":
				retval = 51;
				break;
			// minor
			case "c":
				retval = 52;
				break;
			case "d":
				retval = 54;
				break;
			case "e":
				retval = 56;
				break;
			case "f":
				retval = 57;
				break;
			case "g":
				retval = 59;
				break;
			case "a":
				retval = 61;
				break;
			case "b":
				retval = 63;
				break;
			// pauses
			case "x":
			case "X":
			case "z":
			case "Z":
				retval = 99;
				break;
			default:
				throw new UnknownAbcNoteException("Unknown tune: " + abcTune + " with modifier: " + abcModifier);
		}
		
		for (int i = 0; i < StringUtil.length(abcModifier); i++) {
			switch (abcModifier.charAt(i)) {
				case '\'':
					retval += 12;
					break;
				case ',':
					retval -= 12;
					break;
				default:
					throw new UnknownAbcNoteException("Unknown modifier: " + abcModifier + " with tune: " + abcTune);
			}
		}
		
		return retval;
	}
	
}
