package jtw.util;

public class NullUtil {
	
	public static boolean noneNull(Object ... objects) {
		boolean retVal = false;
		if (objects != null) {
			retVal = true;
			for (Object o : objects) {
				retVal &= o != null;
			}
		}
		return retVal;
	}
	
	public static boolean allNull(Object ... objects) {
		boolean retVal = true;
		for (Object o : objects) {
			retVal &= o == null;
		}
		return retVal;
	}
	
}
