package jtw.util;

/**
 * An interface to convert an object to a double value.
 * 
 * @author Jan-Thierry Wegener
 */
public interface ObjectToDouble<E> {

	public double objToDouble(E o);
	
}
