package jtw.util;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class PaintUtil {

	/**
	 * Draws a line on the given graphics. The line is only drawn when neither the
	 * start nor the end point is <code>null</code>.
	 * 
	 * @param g
	 *            The graphics to paint on. Must not be null.
	 * @param start
	 *            The start point. Can be null.
	 * @param end
	 *            The end point. Can be null.
	 */
	public static void drawLine(Graphics2D g, Point2D start, Point2D end) {
		if (start != null && end != null) {
			g.drawLine((int) (start.getX() + 0.5), (int) (start.getY() + 0.5), (int) (end.getX() + 0.5), (int) (end.getY() + 0.5));
		}
	}
	
	public static void drawLine(Graphics2D g, double x1, double y1, double x2, double y2) {
		g.drawLine((int) (x1 + 0.5), (int) (y1 + 0.5), (int) (x2 + 0.5), (int) (y2 + 0.5));
	}
	
	/**
	 * Draws a rectangle on the given graphics. The rectangle is only drawn when the given rectangle is not <code>null</code>.
	 * 
	 * @param g
	 *            The graphics to paint on. Must not be null.
	 * @param r The rectangle to draw. Can be null.
	 */
	public static void drawRect(Graphics2D g, Rectangle2D r) {
		if (r != null) {
			g.drawRect((int) (r.getX() + 0.5), (int) (r.getY() + 0.5), (int) (r.getWidth() + 0.5), (int) (r.getHeight() + 0.5));
		}
	}

	/**
	 * Draws the string to the given graphics object.
	 * 
	 * @param g The graphics object to paint onto.
	 * @param str The string to draw.
	 * @param x The x-coordinates of the string position.
	 * @param y The y-coordinates of the string position.
	 * @param correctWidth <code>true</code> when the given position shall be the right corner instead of the left corner.
	 * @param correctHeight <code>true</code> when the given position shall be the upper corner instead of the bottom corner.
	 */
	public static void drawString(Graphics2D g, String str, double x, double y, boolean correctWidth, boolean correctHeight) {
		if (correctHeight) {
			y += g.getFontMetrics().getHeight();
		} 
		if (correctWidth) {
			x -= g.getFontMetrics().stringWidth(str);
		}
		g.drawString(str, (float) x, (float) y);
	}
	
	/**
	 * Draws the string to the given graphics object. The given position is the lower left corner.
	 * 
	 * @param g The graphics object to paint onto.
	 * @param str The string to draw.
	 * @param x The x-coordinates of the string position.
	 * @param y The y-coordinates of the string position.
	 */
	public static void drawString(Graphics2D g, String str, double x, double y) {
		drawString(g, str, x, y, false, false);
	}
	

}
