package jtw.util;

public class Pair<V, W> {
	
	private final V first;
	private final W second;
	
	public Pair(V first, W second) {
		this.first = first;
		this.second = second;
	}
	
	public V getFirst() {
		return first;
	}
	
	public W getSecond() {
		return second;
	}
	
	@Override
	public String toString() {
		return "Pair [first=" + first + ", second=" + second + "]";
	}
	
}
