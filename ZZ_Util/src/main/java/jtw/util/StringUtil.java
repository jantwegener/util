package jtw.util;

public final class StringUtil {
	
	private StringUtil() {
		// avoid instantiation
	}
	
	/**
	 * Checks whether the given string is empty (or null).
	 * 
	 * @param str The string to check.
	 * @param isConsiderBlankAsEmpty true if a string of whitespaces is considered as empty, false otherwise.
	 * 
	 * @return true if the string is empty, false otherwise.
	 */
	public static boolean isEmpty(String str, boolean isConsiderBlankAsEmpty) {
		return str == null || str.isEmpty() || (isConsiderBlankAsEmpty && str.trim().isEmpty());
	}
	
	/**
	 * Returns the length of the string.
	 * 
	 * @param str The string whose lenght to return.
	 * 
	 * @return The length of the string; null has the length 0.
	 */
	public static int length(String str) {
		return str == null ? 0 : str.length();
	}
	
	public static String toString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString();
	}
	
}
