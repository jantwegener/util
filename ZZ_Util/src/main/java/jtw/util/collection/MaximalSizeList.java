package jtw.util.collection;

import java.util.List;

/**
 * This interface marks the list as a maximal size list which means that the maximal size of the
 * list cannot be changed after the initialization. When the maximal size is x and more than x
 * elements are added the oldest elements are silently removed.
 * 
 * @author wegener
 *
 * @param <T>
 */
public interface MaximalSizeList<T> extends List<T> {
	
	/**
	 * Returns the number of elements that can still be added without removing any element.
	 * 
	 * @return The number of elements that can still be added without removing any element.
	 */
	public int rest();
	
	/**
	 * Returns the maximal size of this list.
	 * 
	 * @return The maximal size of this list.
	 */
	public int getMaximalSize();
	
}
