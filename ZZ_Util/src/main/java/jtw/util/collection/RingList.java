package jtw.util.collection;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * 
 *
 * <pre>
 * if (start <= end) {
 *     // [ x x x s y y e x x x x ] = 2
 *     rest = end - start;
 * } else {
 *     // [ y y y x x y y y y ]
 *     // [ - - - e - s - - - ]
 *     // m = 9, e = 3, s = 5
 *     // 9 - 5 + 3 = 7
 *     rest = maximalSize - start + end;
 * }
 * </pre>
 * 
 * @author wegener
 *
 * @param <T>
 */
public class RingList<T> implements MaximalSizeList<T> {
	
	private final int maximalSize;
	
	private final T[] entryArray;
	
	/**
	 * Points to the first element in the array.
	 */
	private int start;
	
	/**
	 * Points at the end of the elements in the array. In fact, this pointer points outside of the
	 * elements, i.e., where the next element will be written.
	 */
	private int end;
	
	/**
	 * The number of elements in the list. This must be kept since the start and end cursors can
	 * point to the same element when the list is empty or when it is full.
	 */
	private int numElements = 0;
	
	@SuppressWarnings("unchecked")
	public RingList(int maximalSize) {
		this.maximalSize = maximalSize;
		
		this.entryArray = (T[]) new Object[maximalSize];
		
		start = 0;
		end = 0;
	}
	
	@Override
	public int size() {
		return numElements;
	}
	
	@Override
	public int rest() {
		return maximalSize - size();
	}
	
	@Override
	public int getMaximalSize() {
		return maximalSize;
	}
	
	@Override
	public boolean add(T e) {
		// the pointer end always points to the next position to write
		entryArray[end] = e;
		
		if (start != end) {
			end++;
		} else {
			end++;
			start++;
		}
		
		if (end >= maximalSize) {
			end = 0;
		}
		if (start >= maximalSize) {
			start = 0;
		}
		
		numElements++;
		if (numElements >= maximalSize) {
			numElements = maximalSize;
		}
		
		return true;
	}
	
	@Override
	public void add(int arg0, T arg1) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean addAll(Collection<? extends T> arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean addAll(int arg0, Collection<? extends T> arg1) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void clear() {
		for (int i = 0; i < maximalSize; i++) {
			entryArray[i] = null;
		}
		
		start = end = numElements = 0;
	}
	
	@Override
	public boolean contains(Object o) {
		for (T e : this) {
			if (o == null ? e == null : o.equals(e)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean containsAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public T get(int index) {
		checkBounds(index);
		
		int curr = getPosInArray(index);
		return entryArray[curr];
	}
	
	@Override
	public int indexOf(Object o) {
		int index = 0;
		Iterator<T> iter = iterator();
		while (iter.hasNext()) {
			T e = iter.next();
			if (o == null ? e == null : o.equals(e)) {
				return index;
			}
			index++;
		}
		return -1;
	}
	
	@Override
	public boolean isEmpty() {
		return numElements == 0;
	}
	
	@Override
	public Iterator<T> iterator() {
		return listIterator();
	}
	
	@Override
	public int lastIndexOf(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public ListIterator<T> listIterator() {
		return new RingListIterator(0);
	}
	
	@Override
	public ListIterator<T> listIterator(int index) {
		return new RingListIterator(index);
	}
	
	@Override
	public boolean remove(Object o) {
		int index = indexOf(o);
		if (index == -1) {
			return false;
		}
		remove(index);
		return true;
	}
	
	/**
	 * This is a costly operation and should be avoided. Use {@link #removeLast()} or
	 * {@link #removeFirst()} whenever possible.
	 * 
	 * @see #removeLast()
	 * @see #removeFirst()
	 */
	@Override
	public T remove(int index) {
		checkBounds(index);
		int curr = getPosInArray(index);
		T ret = entryArray[curr];
		
		Iterator<T> iter = listIterator(index);
		// skip next
		if (iter.hasNext()) {
			iter.next();
		}
		
		while (iter.hasNext()) {
			T e = iter.next();
			entryArray[curr] = e;
			curr = increment(curr);
		}
		// remove the pointer to the last element so that it can be cleaned up by garbage collection
		entryArray[curr] = null;
		
		end = decrement(end);
		numElements--;
		
		return ret;
	}
	
	public T removeLast() {
		end = decrement(end);
		T ret = entryArray[end];
		entryArray[end] = null;
		numElements--;
		return ret;
	}
	
	public T removeFirst() {
		T ret = entryArray[start];
		entryArray[start] = null;
		start = increment(start);
		numElements--;
		return ret;
	}
	
	@Override
	public boolean removeAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean retainAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public T set(int arg0, T arg1) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<T> subList(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@SuppressWarnings("hiding")
	@Override
	public <T> T[] toArray(T[] arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(2 * maximalSize);
		
		builder.append('[');
		Iterator<T> iter = this.iterator();
		while (iter.hasNext()) {
			T e = iter.next();
			builder.append(e).append(", ");
		}
		if (builder.length() > 2) {
			builder.delete(builder.length() - 2, builder.length());
		}
		builder.append(']');
		
		return builder.toString();
	}
	
	public String toRawString() {
		return Arrays.toString(entryArray);
	}
	
	/**
	 * Checks if the index is within the bounds 0 and maximalSize.
	 * 
	 * @param index
	 *            The index to check.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             If the index is not in the valid bounds.
	 */
	private void checkBounds(int index) {
		if (index < 0 || index >= maximalSize) {
			throw new IndexOutOfBoundsException(String.format("The index must be within [%d, %d] but is %d.", 0, maximalSize, index));
		}
	}
	
	/**
	 * Returns the position of the given index in the entries array. The validity is not checked.
	 * 
	 * @param index
	 *            The index in the list.
	 * 
	 * @return The cursor position corresponding to the given index.
	 */
	private int getPosInArray(int index) {
		int pos = start + index;
		if (pos >= maximalSize) {
			pos = pos - maximalSize;
		}
		return pos;
	}
	
	private int increment(int index) {
		index++;
		if (index >= maximalSize) {
			index = 0;
		}
		return index;
	}
	
	private int decrement(int index) {
		index--;
		if (index < 0) {
			index = maximalSize - 1;
		}
		return index;
	}
	
	private class RingListIterator implements ListIterator<T> {
		
		private int curr;
		
		private int availableNext;
		
		public RingListIterator(int index) {
			checkBounds(index);
			
			curr = getPosInArray(index);
			availableNext = numElements - index;
		}
		
		@Override
		public void add(T e) {
			throw new UnsupportedOperationException();
		}
		
		@Override
		public boolean hasNext() {
			return availableNext > 0;
		}
		
		@Override
		public boolean hasPrevious() {
			throw new UnsupportedOperationException();
		}
		
		@Override
		public T next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}
			T next = entryArray[curr++];
			
			if (curr >= maximalSize) {
				curr = 0;
			}
			
			availableNext--;
			
			return next;
		}
		
		@Override
		public int nextIndex() {
			throw new UnsupportedOperationException();
		}
		
		@Override
		public T previous() {
			throw new UnsupportedOperationException();
		}
		
		@Override
		public int previousIndex() {
			throw new UnsupportedOperationException();
		}
		
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
		@Override
		public void set(T e) {
			throw new UnsupportedOperationException();
		}
		
	}
	
}
