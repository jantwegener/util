package jtw.util.collection;

public class RingListTest {
	
	public static void main(String[] args) {
		RingList<Integer> list = new RingList<>(5);
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		list.add(6);
		list.add(7);
		
		System.out.println(list);
		
		Integer removed = list.remove(2);
		System.out.println(list + " *" + removed);
		
		removed = list.removeLast();
		System.out.println(list + " *" + removed);
		
		removed = list.removeFirst();
		System.out.println(list + " *" + removed);
		
		System.out.println(list.toRawString());
	}
	
}
