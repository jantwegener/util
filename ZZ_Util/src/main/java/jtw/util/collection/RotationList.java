package jtw.util.collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

/**
 * Rotates around until it returns to the starting position.
 * 
 * @author Jan-Thierry Wegener
 *
 * @param <E> Elements of the list.
 */
public class RotationList<E> extends ArrayList<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int startingPosition = 0;
	
	public RotationList() {
		super();
	}
	
	public void setStartingPosition(int startingPosition) {
		this.startingPosition = startingPosition;
	}
	
	/**
	 * Increments the pointer to the next element.
	 * 
	 * @return true if the list looped from the last to the first element, false otherwise.
	 */
	public boolean increment() {
		startingPosition++;
		if (startingPosition >= size()) {
			startingPosition = 0;
			return true;
		}
		return false;
	}
	
	/**
	 * Returns the number of increments before looping to the first element.
	 * 
	 * @return The number of increments before looping to the first element.
	 */
	public int incrementBeforeLoop() {
		return size() - 1 - startingPosition;
	}
	
	/**
	 * Decrements the pointer to the previous element.
	 * 
	 * @return true if the list looped from the first to the last element, false otherwise.
	 */
	public boolean decrement() {
		startingPosition--;
		if (startingPosition < 0) {
			startingPosition = size() - 1;
			return true;
		}
		return false;
	}
	
	/**
	 * Returns the number of decrements before looping to the last element.
	 * 
	 * @return The number of decrement before looping to the last element.
	 */
	public int decrementBeforeLoop() {
		return startingPosition;
	}
	
	/**
	 * Returns the current element.
	 * 
	 * @return The current element.
	 */
	public E get() {
		return get(startingPosition);
	}
	
	/**
	 * Returns the current position in the list.
	 * 
	 * @return The current position in the list.
	 */
	public int getPosition() {
		return startingPosition;
	}
	
	@Override
	public Iterator<E> iterator() {
		return new Iter();
	}
	
	@Override
	public ListIterator<E> listIterator() {
		return new Iter();
	}
	
	private class Iter implements ListIterator<E> {
		
		int curr = startingPosition;
		int lastRet = -1;
		boolean isStarted = false;
		
		@Override
		public boolean hasNext() {
			if (isStarted) {
				return curr != startingPosition;
			}
			return !isEmpty();
		}

		@Override
		public E next() {
			isStarted = true;
			
			E retval = get(curr);
			lastRet = curr;
			curr++;
			
			if (curr >= size()) {
				curr = 0;
			}
			
			return retval;
		}

		@Override
		public boolean hasPrevious() {
			if (isStarted) {
				return curr != startingPosition;
			}
			return !isEmpty();
		}

		@Override
		public E previous() {
			isStarted = true;
			
			curr--;
			if (curr < 0) {
				curr = size() - 1;
			}
			E retval = get(curr);
			lastRet = curr;
			
			return retval;
		}

		@Override
		public int nextIndex() {
			return curr;
		}

		@Override
		public int previousIndex() {
			return curr - 1;
		}

		@Override
		public void remove() {
            if (lastRet < 0) {
            	throw new IllegalStateException();
            }
            RotationList.this.remove(lastRet);
            curr = lastRet;
            lastRet = -1;
		}

		@Override
		public void set(E e) {
            if (lastRet < 0) {
            	throw new IllegalStateException();
            }
            RotationList.this.set(lastRet, e);
		}

		@Override
		public void add(E e) {
            if (lastRet < 0) {
            	throw new IllegalStateException();
            }
            RotationList.this.add(lastRet, e);
		}
		
	}
	
}
