package jtw.util.color;

import java.awt.Color;

public class ColorTools {
	
	public static int[] getRGB(int rgb) {
		int[] val = new int[3];
		
		val[0] = (0x00ff0000 & rgb) >> 16;
		val[1] = (0x0000ff00 & rgb) >> 8;
		val[2] = (0x000000ff & rgb) >> 0;
		
		return val;
	}
	
	public static float[] RGBtoHSV(int color) {
		int[] rgb = getRGB(color);
		
		return Color.RGBtoHSB(rgb[0], rgb[1], rgb[2], null);
	}
	
	public static int HSVtoRGB(float[] hsv) {
		return Color.HSBtoRGB(hsv[0], hsv[1], hsv[2]);
	}
	
}
