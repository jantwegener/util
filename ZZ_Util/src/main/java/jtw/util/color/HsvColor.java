package jtw.util.color;

import java.awt.Color;
import java.util.Arrays;

public class HsvColor extends Color {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private float[] hsv = new float[3];
	
	public HsvColor(Color color) {
		this(color.getRed(), color.getGreen(), color.getBlue());
	}
	
	public HsvColor(int r, int g, int b) {
		this(((r & 0xFF) << 16) |
				((g & 0xFF) << 8) |
				((b & 0xFF) << 0));
	}
	
	public HsvColor(int rgb) {
		super(rgb);
		int r = (rgb >> 16) & 0xFF;
		int g = (rgb >>  8) & 0xFF;
		int b = (rgb >>  0) & 0xFF;
		Color.RGBtoHSB(r, g, b, hsv);
	}
	
	public HsvColor(float h, float s, float v) {
		this(Color.getHSBColor(h, s, v).getRGB());
	}
	
	@Override
	public HsvColor brighter() {
		return new HsvColor(super.brighter());
	}
	
	@Override
	public HsvColor darker() {
		return new HsvColor(super.darker());
	}
	
	public float[] getHsv() {
		return hsv;
	}
	
	public float getHue() {
		return hsv[0];
	}
	
	public float getSaturation() {
		return hsv[1];
	}
	
	public float getValue() {
		return hsv[2];
	}
	
	@Override
	public String toString() {
		return "HsvColor [hsv=" + Arrays.toString(hsv) + "]";
	}
	
}
