package jtw.util.exception;

public class UnknownAbcNoteException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UnknownAbcNoteException() {
		super();
	}
	
	public UnknownAbcNoteException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
	
	public UnknownAbcNoteException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public UnknownAbcNoteException(String message) {
		super(message);
	}
	
	public UnknownAbcNoteException(Throwable cause) {
		super(cause);
	}
	
}
