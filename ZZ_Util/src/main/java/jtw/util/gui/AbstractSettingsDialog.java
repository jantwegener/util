package jtw.util.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 * 
 * @author Jan-Thierry Wegener
 */
public abstract class AbstractSettingsDialog<T> extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final int NO_OPTION = JOptionPane.NO_OPTION;
	public static final int CANCEL_OPTION = NO_OPTION;

	public static final int YES_OPTION = JOptionPane.YES_OPTION;
	public static final int OK_OPTION = YES_OPTION;
	
	private JButton buttonOk = new JButton("OK");
	private JButton buttonCancel = new JButton("Cancel");
	
	private int buttonPressed = NO_OPTION;
	
	private final int hgap;
	private final int vgap;
	
	private final boolean okOnly;
	
	private final String initialTitle;
	private String customTitle;

	public AbstractSettingsDialog(JFrame parent, String title, boolean modal, boolean okOnly, int hgap, int vgap, Object ... additionalValues) {
		super(parent, title, modal);
		
		this.initialTitle = title;
		this.hgap = hgap;
		this.vgap = vgap;
		this.okOnly = okOnly;
		
		initMenu();
		
		if (additionalValues != null) {
			init(additionalValues);
		}
		
		initPanel();
	}
	
	public AbstractSettingsDialog(JDialog parent, String title, boolean modal, boolean okOnly, int hgap, int vgap, Object ... additionalValues) {
		super(parent, title, modal);
		
		this.initialTitle = title;
		this.hgap = hgap;
		this.vgap = vgap;
		this.okOnly = okOnly;
		
		initMenu();
		
		if (additionalValues != null) {
			init(additionalValues);
		}
		
		initPanel();
	}
	
	public AbstractSettingsDialog(JFrame parent, String title, boolean modal, boolean okOnly) {
		this(parent, title, modal, okOnly, 10, 5);
	}
	
	public AbstractSettingsDialog(JFrame parent, String title, boolean modal) {
		this(parent, title, modal, false, 10, 5);
	}
	
	public AbstractSettingsDialog(String title, boolean modal) {
		this((JFrame) null, title, modal);
	}
	
	/**
	 * Returns the result when pressing the OK button.
	 * 
	 * @return The result of the dialog.
	 */
	public abstract T getResult();
	
	protected void init(Object[] additionalValues) {
	}
	
	/**
	 * Initializes the panels.
	 */
	protected void initPanel() {
		JPanel main = new JPanel(new BorderLayout(hgap, vgap));

		JPanel south = initSouth();
		
		JPanel east = initEast();
		
		JPanel west = initWest();
		
		JPanel center = initCenter();
		
		JPanel north = initNorth();

		if (north != null) {
			main.add(north, BorderLayout.NORTH);
		}
		if (east != null) {
			main.add(east, BorderLayout.EAST);
		}
		if (west != null) {
			main.add(west, BorderLayout.WEST);
		}
		if (center != null) {
			main.add(center, BorderLayout.CENTER);
		}
		if (south != null) {
			main.add(south, BorderLayout.SOUTH);
		}
		
		add(main);
	}
	
	protected JPanel initNorth() {
		return null;
	}
	
	/**
	 * The main panel for the settings dialog. Or null if nothing shall be in the center.
	 * 
	 * @return The main panel. Can be <code>null</code> if nothing shall appear in the center.
	 */
	protected abstract JPanel initCenter();

	protected JPanel initWest() {
		return null;
	}

	protected JPanel initEast() {
		return null;
	}

	protected JPanel initSouth() {
		if (!okOnly) {
			buttonCancel.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					buttonPressed = NO_OPTION;
					setVisible(false);
					dispose();
				}
			});
		}
		
		ActionListener okAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonPressed = YES_OPTION;
				setVisible(false);
				dispose();
			}
		};
		
		buttonOk.addActionListener(okAction);
		
		JPanel south = new JPanel(new FlowLayout(FlowLayout.CENTER));
		south.setName("SOUTH");
		south.add(buttonOk);
		if (!okOnly) {
			south.add(buttonCancel);
		}

		return south;
	}
	
	public int getButtonPressed() {
		return buttonPressed;
	}
	
	public void showDialog() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		validate();
		pack();
		setLocationRelativeTo(getParent());
		setVisible(true);
	}

	protected void initMenu() {
		JMenuBar bar = new JMenuBar();
		
		JMenu fileMenu = new JMenu("File");
		fileMenu.setName("File Menu");
		JMenuItem changeTitleMenuItem = new JMenuItem("Change Title...");
		changeTitleMenuItem.setName("Change Title Menu Item");
		changeTitleMenuItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				customTitle = JOptionPane.showInputDialog(getParent(), "Enter a new title", "New title...", JOptionPane.QUESTION_MESSAGE);
				String title;
				if (customTitle != null && !customTitle.trim().isEmpty()) {
					title = String.format("%s (%s)", initialTitle, customTitle);
				} else {
					title = initialTitle;
					customTitle = null;
				}
				setTitle(title);
			}
		});
		changeTitleMenuItem.setAccelerator(KeyStroke.getKeyStroke("control T"));
		
		fileMenu.add(changeTitleMenuItem);
		bar.add(fileMenu);
		
		initAdditionalMenus(bar, fileMenu);
		
		setJMenuBar(bar);
	}
	
	/**
	 * Returns the new title or null if none was set.
	 * 
	 * @return The new title.
	 */
	protected String getCustomizedTitle() {
		return customTitle;
	}
	
	/**
	 * Can be used to add some more menus and menu items. This method is called before the bar is set to the {@link JDialog}.
	 * 
	 * @param bar The menu bar.
	 * @param fileMenu The standard menu "File".
	 */
	protected void initAdditionalMenus(JMenuBar bar, JMenu fileMenu) {
	}
	
}
