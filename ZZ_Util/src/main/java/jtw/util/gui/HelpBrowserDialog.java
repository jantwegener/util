package jtw.util.gui;

import java.awt.BorderLayout;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class HelpBrowserDialog extends AbstractSettingsDialog<Object> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private URL initialPage;
	
	public HelpBrowserDialog(JFrame parent, String title, URL initialPage) {
		super(parent, title, false, true, 10, 5, initialPage);
	}
	
	public HelpBrowserDialog(JFrame parent, String title, File initialPageFile) throws MalformedURLException {
		super(parent, title, false, true, 10, 5, initialPageFile.toURI().toURL());
	}
	
	public HelpBrowserDialog(JFrame parent, String title, String initialPageFile) throws MalformedURLException {
		this(parent, title, new File(initialPageFile));
	}
	
	public HelpBrowserDialog(JDialog parent, String title, URL initialPage) {
		super(parent, title, false, true, 10, 5, initialPage);
	}
	
	@Override
	protected void init(Object[] additionalValues) {
		if (additionalValues != null && additionalValues.length >= 1 && additionalValues[0] instanceof URL) {
			this.initialPage = (URL) additionalValues[0];
		}
	}
	
	@Override
	public Object getResult() {
		return null;
	}
	
	@Override
	protected JPanel initCenter() {
		JPanel mainPanel = new JPanel(new BorderLayout());
		
		try {
			JEditorPane editorPane = new JEditorPane(initialPage);
			editorPane.setEditable(false);
			
			JScrollPane scrollPane = new JScrollPane(editorPane);
			
			mainPanel.add(scrollPane, BorderLayout.CENTER);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
		return mainPanel;
	}
	
}
