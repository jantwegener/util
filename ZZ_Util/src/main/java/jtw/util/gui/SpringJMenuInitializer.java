package jtw.util.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.StringUtils;

public class SpringJMenuInitializer {
	
	public static final String DEFAULT_ENABLED_STATE = "JTW_DefaultEnabledState";
	
	private final List<Action> actionWithDefaultEnabledStateList = new ArrayList<>();
	
	private final Map<String, JMenu> idMenuMap = new HashMap<>();

	public SpringJMenuInitializer() {
	}
	
	/**
	 * Returns a list with all the actions that have a default enabled state.
	 * 
	 * @return A list. Can be empty.
	 */
	public List<Action> getActionWithDefaultEnabledStateList() {
		return actionWithDefaultEnabledStateList;
	}
	
	@SuppressWarnings("unchecked")
	public void addMenu(ApplicationContext context, String prefix, String beanNameMenu, JMenuBar bar, JMenu parentMenu) throws IOException {
		String[] textAndMnemonicArray = beanNameMenu.split("#", 2);
		String mnemonic = null;
		String menuName = null;
		if (textAndMnemonicArray.length >= 2) {
			menuName = textAndMnemonicArray[0];
			mnemonic = textAndMnemonicArray[1];
		} else {
			menuName = textAndMnemonicArray[0];
		}
		
		JMenu menu = new JMenu(menuName.replaceAll("_", " "));
		menu.setName(prefix + "." + menuName);
		if (!StringUtils.isEmpty(mnemonic)) {
			menu.setMnemonic(mnemonic.charAt(0));
		}
		
		String menuId = prefix + "." + menuName;
		idMenuMap.put(menuId, menu);

		if (bar != null) {
			bar.add(menu);
		} else if (parentMenu != null) {
			parentMenu.add(menu);
		} else {
			throw new IOException("Either bar or parentMenu must not be null.");
		}

		String beanDefMenuList = prefix + "." + menuName + ".menuNameList";
		if (!context.containsBean(beanDefMenuList)) {
			throw new IOException("Cannot find the bean '" + beanDefMenuList + "' needed for a menu.");
		}

		List<String> menuList = (List<String>) context.getBean(beanDefMenuList);
		for (String subMenuItemName : menuList) {
			if ("-".equals(subMenuItemName.trim())) {
				menu.addSeparator();
			} else {
				String nextItemBeanName = subMenuItemName.split("#")[0];
				String nextPrefix = prefix + "." + menuName;
				
				boolean isMenu = context.containsBean(nextPrefix + "." + nextItemBeanName + ".menuNameList");
				if (isMenu) {
					addMenu(context, nextPrefix, subMenuItemName, null, menu);
				} else {
					addMenuItem(context, nextPrefix, subMenuItemName, menu);
				}
			}
		}
	}
	
	/**
	 * Returns the menu with the given id or null if none is found.
	 * 
	 * @param id The id of the menu.
	 * 
	 * @return The menu for the given id.
	 */
	public JMenu getMenu(String id) {
		return idMenuMap.get(id);
	}
	
	private void addMenuItem(ApplicationContext context, String prefix, String beanItemName, JMenu parentMenu) {
		String[] textAndMnemonicArray = beanItemName.split("#", 3);
		String mnemonic = null;
		if (textAndMnemonicArray.length >= 2) {
			beanItemName = textAndMnemonicArray[0];
			mnemonic = textAndMnemonicArray[1];
		}
		JMenuItem item = (JMenuItem) context.getBean(prefix + "." + beanItemName);
		item.setName(prefix + "." + beanItemName);
		if (StringUtils.isEmpty(item.getText())) {
			item.setText(beanItemName);
		}
		if (!StringUtils.isEmpty(mnemonic)) {
			item.setMnemonic(mnemonic.charAt(0));
			if (textAndMnemonicArray.length >= 3) {
				item.setAccelerator(KeyStroke.getKeyStroke(textAndMnemonicArray[2]));
			}
		}
		if (item.getAction() != null && item.getAction().getValue(DEFAULT_ENABLED_STATE) != null) {
			actionWithDefaultEnabledStateList.add(item.getAction());
		}
		parentMenu.add(item);
	}
	
	@SuppressWarnings("unchecked")
	public JMenuBar getJMenuBar(String menuXMLFile) throws IOException {
		JMenuBar bar = new JMenuBar();
		try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] {menuXMLFile})) {
			List<String> menuList = (List<String>) context.getBean("jtw.menu.menuNameList");
			for (String menuName : menuList) {
				addMenu(context, "jtw.menu", menuName, bar, null);
			}
		}
		return bar;
	}
	
	@SuppressWarnings("unchecked")
	public JMenuBar getJMenuBar(ApplicationContext context) throws IOException {
		JMenuBar bar = new JMenuBar();
		List<String> menuList = (List<String>) context.getBean("jtw.menu.menuNameList");
		for (String menuName : menuList) {
			addMenu(context, "jtw.menu", menuName, bar, null);
		}
		return bar;
	}
	
}
