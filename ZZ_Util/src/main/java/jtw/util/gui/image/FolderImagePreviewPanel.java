package jtw.util.gui.image;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class FolderImagePreviewPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * The array of images loaded.
	 */
	private Image[] imageArray;
	
	/**
	 * The name of the image (filename).
	 */
	private String[] imageNameArray;
	
	/**
	 * The folder path.
	 */
	private File path;
	
	/**
	 * The number of images shown on the x-axis. It is 1 by default to avoid division by zero exceptions.
	 */
	private int numGridX = 1;
	
	/**
	 * Space on the left, top, right, bottom.
	 */
	private int inset = 12;
	
	/**
	 * The selected index.
	 */
	private int selectedIndex = -1;
	
	private ImageObserver observer = null;
	
	public FolderImagePreviewPanel() {
	}
	
	public FolderImagePreviewPanel(String path, int numGridX) {
		this(new File(path), numGridX);
	}
	
	public FolderImagePreviewPanel(File path, int numGridX) {
		// use the method to ensure having a directory
		setFolder(path);
		// and set the numGridX value
		this.numGridX = numGridX;
	}
	
	public int getNumGridX() {
		return numGridX;
	}
	
	/**
	 * Sets the grids on the horizontal to show.
	 * 
	 * @param numGridX The number of images to show on the horizontal.
	 */
	public void setNumGridX(int numGridX) {
		this.numGridX = numGridX;
	}
	
	/**
	 * Sets the selected index.
	 * 
	 * @param index The new selected index.
	 */
	public void setSelectedIndex(int index) {
		this.selectedIndex = index;
	}
	
	/**
	 * Sets the selected index by the file name.
	 * 
	 * @param fileName The filename of the selected image.
	 */
	public void setSelectedIndexByFileName(String fileName) {
		for (int i = 0; i < imageNameArray.length; i++) {
			if (imageNameArray[i].equals(fileName)) {
				selectedIndex = i;
				break;
			}
		}
	}
	
	/**
	 * Sets the selected index from the given cell.
	 * 
	 * @param gridX The x-coordinate of the cell.
	 * @param gridY The y-coordinate of the cell.
	 */
	public void setSelectedIndex(int gridX, int gridY) {
		selectedIndex = gridX + gridY * numGridX;
		if (selectedIndex >= imageArray.length) {
			selectedIndex = imageArray.length - 1;
		}
	}
	
	/**
	 * Sets the selected index from the given cell.
	 * 
	 * @param coords The (x, y)-coordinate of the cell.
	 */
	public void setSelectedIndex(int[] coords) {
		setSelectedIndex(coords[0], coords[1]);
	}
	
	/**
	 * Returns the selected index.
	 * 
	 * @return The selected index.
	 */
	public int getSelectedIndex() {
		return selectedIndex;
	}
	
	/**
	 * Returns the coordinates of the selected cell.
	 * 
	 * @return The coordinates of the selected cell.
	 */
	public int[] getSelectedCellCoords() {
		int x = selectedIndex % numGridX;
		int y = selectedIndex / numGridX;
		return new int[] { x, y };
	}
	
	/**
	 * Sets a new path to load the images from.
	 * 
	 * @param path The new path.
	 */
	public void setFolder(File path) {
		if (path.isFile()) {
			path = path.getParentFile();
		}
		this.path = path;
	}
	
	/**
	 * Sets a new path to load the images from.
	 * 
	 * @param path The new path.
	 */
	public void setFolder(String path) {
		setFolder(new File(path));
	}
	
	/**
	 * Loads the images from the set path.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void loadImages() throws IOException {
		// load the images from the folder
		File[] allImageFileArray = path.listFiles(file ->  {return file.getName().toLowerCase().endsWith(".png") || file.getName().toLowerCase().endsWith(".jpg") || file.getName().toLowerCase().endsWith(".gif");});
		imageArray = new Image[allImageFileArray.length];
		imageNameArray = new String[allImageFileArray.length];
		for (int i = 0; i < imageArray.length; i++) {
			imageArray[i] = ImageIO.read(allImageFileArray[i]);
			imageNameArray[i] = allImageFileArray[i].getName();
		}
	}
	
	/**
	 * Returns the grid corresponding to the given pixel coordinates.
	 * 
	 * @param x The pixel in the panel.
	 * @param y The pixel in the panel.
	 * 
	 * @return The corresponding grid.
	 */
	public int[] getGridFromPixelCoords(int x, int y) {
		int numGridY = (int) Math.ceil(imageArray.length / (double) numGridX);
		int w = getWidth() / numGridX - inset * numGridX;
		int h = getHeight() / numGridY - inset * numGridY;
		
		int gx = x / (w + inset);
		int gy = y / (h + inset);
		return new int[] { gx, gy };
	}
	
	/**
	 * Returns the image at the given cell.
	 * 
	 * @param coords The (x, y)-coordinate of the cell.
	 * 
	 * @return The image at the given grid.
	 */
	public Image getImage(int[] coords) {
		return getImage(coords[0], coords[1]);
	}
	
	/**
	 * Returns the image at the given cell.
	 * 
	 * @param gridX The x-coordinate of the cell.
	 * @param gridY The y-coordinate of the cell.
	 * 
	 * @return The image at the given grid.
	 */
	public Image getImage(int gridX, int gridY) {
		int index = gridX + gridY * numGridX;
		if (index >= imageArray.length) {
			index = imageArray.length - 1;
		}
		return imageArray[index];
	}
	
	/**
	 * Returns the currently selected image.
	 * 
	 * @return The selected image or null if none is selected.
	 */
	public Image getSelectedImage() {
		Image retImg = null;
		if (selectedIndex >= 0) {
			retImg = imageArray[selectedIndex];
		}
		return retImg;
	}
	
	/**
	 * Returns the name of the image at the given cell.
	 * 
	 * @param coords The (x, y)-coordinate of the cell.
	 * 
	 * @return The image name.
	 */
	public String getImageName(int[] coords) {
		return getImageName(coords[0], coords[1]);
	}
	
	/**
	 * Returns the name of the image at the given cell.
	 * 
	 * @param gridX The x-coordinate of the cell.
	 * @param gridY The y-coordinate of the cell.
	 * 
	 * @return The image name.
	 */
	public String getImageName(int gridX, int gridY) {
		int index = gridX + gridY * numGridX;
		if (index >= imageArray.length) {
			index = imageArray.length - 1;
		}
		return imageNameArray[index];
	}
	
	/**
	 * Returns the name of the currently selected image.
	 * 
	 * @return The name of the selected image or null if none is selected.
	 */
	public String getSelectedImageName() {
		String retImg = null;
		if (selectedIndex >= 0) {
			retImg = imageNameArray[selectedIndex];
		}
		return retImg;
	}
	
	public void paintComponent(Graphics g) {
		if (imageArray != null) {
			g.setColor(Color.LIGHT_GRAY);
			g.fillRect(0, 0, getWidth(), getHeight());
			
			int numGridY = (int) Math.ceil(imageArray.length / (double) numGridX);
			int imgWidth = getWidth() / numGridX - inset * numGridX;
			int imgHeight = getHeight() / numGridY - inset * numGridY;
			
			for (int xi = 0; xi < numGridX; xi++) {
				for (int yi = 0; yi < numGridY; yi++) {
					int imgIndex = xi + yi * numGridX;
					if (imgIndex < imageArray.length) {
						if (imgIndex == selectedIndex) {
							g.setColor(Color.YELLOW);
							g.fillRect(xi * (inset + imgWidth), yi * (inset + imgHeight), imgWidth + 2 * inset, imgHeight + 2 * inset);
						}
						g.drawImage(imageArray[imgIndex], inset + xi * (inset + imgWidth), inset + yi * (inset + imgHeight), imgWidth, imgHeight, observer);
						g.setColor(Color.BLACK);
						g.drawString(imageNameArray[imgIndex], inset + xi * (inset + imgWidth), inset + yi * (inset + imgHeight));
					}
				}
			}
		} else {
			g.setColor(Color.DARK_GRAY);
			g.fillRect(0, 0, getWidth() / 2, getHeight() / 2);
			g.fillRect(getWidth() / 2, getHeight() / 2, getWidth() / 2, getHeight() / 2);
		}
	}
	
	public int getInset() {
		return inset;
	}
	
	/**
	 * Sets the insets between the images.
	 * 
	 * @param inset The new insets.
	 */
	public void setInset(int inset) {
		this.inset = inset;
	}
	
}
