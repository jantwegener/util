package jtw.util.gui.image;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;

import javax.swing.JPanel;

public class ImagePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Image image = null;
	
	private ImageObserver observer = null;
	
	private ScreenPainter screenPainter = null;
	
	/**
	 * Creates a new panel without any image set.
	 */
	public ImagePanel() {
		this(null, null);
	}
	
	/**
	 * Creates a new panel showing the given image.
	 * 
	 * @param img The image to show.
	 */
	public ImagePanel(Image img) {
		this(img, null);
	}
	
	public ImagePanel(Image image, ImageObserver observer) {
		this.image = image;
		this.observer = observer;
	}
	
	public Image getImage() {
		return image;
	}
	
	public void setScreenPainter(ScreenPainter painter) {
		screenPainter = painter;
	}
	
	/**
	 * Sets an image to paint on the panel.
	 * 
	 * @param image The new image.
	 */
	public void setImage(Image image) {
		this.image = image;
	}
	
	public ImageObserver getObserver() {
		return observer;
	}
	
	public void setObserver(ImageObserver observer) {
		this.observer = observer;
	}
	
	public void paintComponent(Graphics g) {
		if (image != null) {
			g.drawImage(image, 0, 0, getWidth(), getHeight(), observer);
			
			if (screenPainter != null) {
				screenPainter.paint(g, getWidth(), getHeight(), image.getWidth(observer), image.getHeight(observer));
			}
		} else {
			g.setColor(Color.DARK_GRAY);
			g.fillRect(0, 0, getWidth() / 2, getHeight() / 2);
			g.fillRect(getWidth() / 2, getHeight() / 2, getWidth() / 2, getHeight() / 2);
		}
	}

}
