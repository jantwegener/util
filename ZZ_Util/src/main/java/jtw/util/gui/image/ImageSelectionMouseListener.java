package jtw.util.gui.image;

import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ImageSelectionMouseListener implements MouseListener {
	
	private ImagePanel imagePanel;
	private FolderImagePreviewPanel previewPanel;
	
	public ImageSelectionMouseListener(FolderImagePreviewPanel previewPanel, ImagePanel imagePanel) {
		this.previewPanel = previewPanel;
		this.imagePanel = imagePanel;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		int[] grid = previewPanel.getGridFromPixelCoords(e.getX(), e.getY());
		Image img = previewPanel.getImage(grid);
		previewPanel.setSelectedIndex(grid);
		previewPanel.repaint();
		imagePanel.setImage(img);
		imagePanel.repaint();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

}
