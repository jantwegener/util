package jtw.util.gui.image;

import java.awt.Graphics;

/**
 * To paint on an {@link ImagePanel}.
 * 
 * @author Jan-Thierry Wegener
 */
public interface ScreenPainter {
	
	/**
	 * 
	 * @param g The graphics object.
	 * @param width The width of the panel.
	 * @param height The height of the panel.
	 * @param imgWidth The width of the panel.
	 * @param imgHeight The height of the panel.
	 */
	public void paint(Graphics g, int width, int height, int imgWidth, int imgHeight);
	
}
