package jtw.util.gui.listener;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ListSelectionListenerAdapter implements ListSelectionListener {
	
	public ListSelectionListenerAdapter() {
	}
	
	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getValueIsAdjusting()) {
			return;
		}
		
		ListSelectionModel lsm;
		if (e.getSource() instanceof ListSelectionModel) {
			lsm = (ListSelectionModel) e.getSource();
		} else if (e.getSource() instanceof JList<?>) {
			lsm = ((JList<?>) e.getSource()).getSelectionModel();
		} else {
			throw new ClassCastException("Cannot cast " + e.getSource().getClass().getName() + " to ListSelectionModel or to JList");
		}
		
		int min = lsm.getMinSelectionIndex();
		int max = lsm.getMaxSelectionIndex();
		List<Integer> indexList = new ArrayList<>();
		for (int i = min; i <= max; i++) {
			if (lsm.isSelectedIndex(i)) {
				indexList.add(i);
			}
		}
		
		if (!indexList.isEmpty()) {
			switch (lsm.getSelectionMode()) {
				case ListSelectionModel.SINGLE_SELECTION:
					valueChanged(e, indexList.get(0));
					break;
				case ListSelectionModel.SINGLE_INTERVAL_SELECTION:
				case ListSelectionModel.MULTIPLE_INTERVAL_SELECTION:
					valueChanged(e, indexList.stream().mapToInt(i -> i.intValue()).toArray());
					break;
				default:
					throw new RuntimeException("Unknown selection mode: " + lsm.getSelectionMode());
			}
		}
	}
	
	/**
	 * Called with the correct index.
	 * 
	 * @param e The original event.
	 * @param changedIndex The changed index if in {@link ListSelectionModel#SINGLE_SELECTION} mode.
	 */
	protected void valueChanged(ListSelectionEvent e, int changedIndex) {
	}
	
	/**
	 * Called with the correct index.
	 * 
	 * @param e The original event.
	 * @param changedIndex The changed indexes if in {@link ListSelectionModel#SINGLE_INTERVAL_SELECTION} or in {@link ListSelectionModel#MULTIPLE_INTERVAL_SELECTION} mode.
	 */
	protected void valueChanged(ListSelectionEvent e, int[] changedIndex) {
	}
	
}
