package jtw.util.io;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.UnsupportedAudioFileException;

public class AudioAuOutputStream extends OutputStream {
	
	/**
	 * Marker if the header is already written.
	 */
	private boolean isHeaderWritten = false;
	
	private AudioFormat format;
	private DataOutputStream out;
	
	/**
	 * The encoding of the data.
	 */
	private int encoding;
	
	public AudioAuOutputStream(OutputStream out, AudioFormat format) throws UnsupportedAudioFileException {
		this.out = new DataOutputStream(out);
		
		this.format = format;
		
		boolean isBigEndian = format.isBigEndian();
		if (!isBigEndian) {
			throw new UnsupportedAudioFileException("The format must be big endian.");
		}
	}
	
	private void writeHeader() throws IOException, UnsupportedAudioFileException {
		int magicNumber = 0x2e736e64;
		int offset = 24;
		// unknown data size
		int dataSize = 0xffffffff;
		encoding = getEncoding();
		int sampleRate = Math.round(format.getSampleRate());
		int channels = format.getChannels();

		out.writeInt(magicNumber);
		out.writeInt(offset);
		out.writeInt(dataSize);
		out.writeInt(encoding);
		out.writeInt(sampleRate);
		out.writeInt(channels);
	}
	
	private int getEncoding() throws UnsupportedAudioFileException {
		int retval = -1;
		
		int sampleSizeInBits = format.getSampleSizeInBits();
		switch (format.getEncoding().toString()) {
			case "PCM_SIGNED":
				switch (sampleSizeInBits) {
					case 8:
						retval = 2;
						break;
					case 16:
						retval = 3;
						break;
					case 24:
						retval = 4;
						break;
					case 32:
						retval = 5;
						break;
				}
				break;
			case "PCM_FLOAT":
				if (sampleSizeInBits == 32) {
					retval = 6;
				} else if (sampleSizeInBits == 64) {
					retval = 7;
				}
				break;
			case "PCM_UNSIGNED":
				// the data is always signed
				throw new UnsupportedAudioFileException("Unsupported encoding: " + format.getEncoding());
			default:
				throw new UnsupportedAudioFileException("Unsupported encoding: " + format.getEncoding());
		}
		
		return retval;
	}
	
	private void writeInt(int v) throws IOException {
        write((v >>> 24) & 0xFF);
        write((v >>> 16) & 0xFF);
        write((v >>>  8) & 0xFF);
        write((v >>>  0) & 0xFF);
    }
    
    private void writeLong(long v) throws IOException {
    	int p1 = (int)((v >>> 32) & 0xFFFFFFFF);
    	int p2 = (int)((v >>>  0) & 0xFFFFFFFF);
    	writeInt(p1);
    	writeInt(p2);
    }
    
    private void writeDouble(double v) throws IOException {
    	writeLong(Double.doubleToLongBits(v));
    }
    
    private void writeFloat(float v) throws IOException {
    	writeInt(Float.floatToIntBits(v));
    }
    
    @Override
    public void write(int b) throws IOException {
    	if (!isHeaderWritten) {
    		try {
				writeHeader();
			} catch (UnsupportedAudioFileException e) {
				throw new IOException(e);
			}
    	}
    	out.write(b);
    }

    private void write8(int num) throws IOException {
    	int val = num & 0x000000FF;
    	if (num < 0) {
    		val = val | 0x80;
    	}
    	write(val);
    }
    
    private void write16(int num) throws IOException {
    	int val = (num >>> 8) & 0xFF;
    	if (num < 0) {
    		val = val | 0x80;
    	}
        out.write(val);
        out.write((num >>> 0) & 0xFF);
    	write(val);
    }
    
    private void write24(int num) throws IOException {
    	int val = (num >>> 16) & 0xFF;
    	if (num < 0) {
    		val = val | 0x80;
    	}
        out.write(val);
        out.write((num >>> 8) & 0xFF);
        out.write((num >>> 0) & 0xFF);
    	write(val);
    }
    
    private void write32(int num) throws IOException {
    	writeInt(num);
    }
    
    /**
     * Writes the next value to the stream.
     * 
     * @param number The next value.
     * @throws IOException 
     */
	public void writeValue(Number number) throws IOException {
		try {
			if (!isHeaderWritten) {
				writeHeader();
			}
		} catch (UnsupportedAudioFileException e) {
			throw new IOException(e);
		}
		
		switch (encoding) {
			case 2:
				write8(number.intValue());
				break;
			case 3:
				write16(number.intValue());
				break;
			case 4:
				write24(number.intValue());
				break;
			case 5:
				write32(number.intValue());
				break;
			case 6:
				writeFloat(number.floatValue());
				break;
			case 7:
				writeDouble(number.doubleValue());
				break;
		}
	}
    
}
