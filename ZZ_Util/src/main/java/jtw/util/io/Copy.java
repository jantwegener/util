package jtw.util.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.DeflaterInputStream;
import java.util.zip.GZIPInputStream;

public class Copy {
	
	private static int BUFFER_SIZE = 8192;
	
	/**
	 * Copies from the input stream to the output stream. The streams are not closed within this
	 * method.
	 * 
	 * @param in
	 *            The input stream to copy from.
	 * @param out
	 *            The output stream to copy to.
	 *            
	 * @return The total number of bytes copied.
	 * 
	 * @throws IOException
	 *             If an {@link IOException} occurs.
	 */
	public static long copy(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[BUFFER_SIZE];
		long total = 0;
		int len = 0;
		while ((len = in.read(buffer)) > 0) {
			out.write(buffer, 0, len);
			total += len;
		}
		return total;
	}

	public static String copyFromURL(String url, String outfile) throws IOException {
		return copyFromURL(url, new File(outfile));
	}
	
	public static String copyFromURL(String url, File outfile) throws IOException {
		try (BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(outfile))) {
			return copyFromURL(url, output);
		}
	}
	
	public static String copyFromURL(String url, OutputStream out) throws IOException {
		URL downloadURL = new URL(url);
		
		HttpURLConnection connection = (HttpURLConnection) downloadURL.openConnection();
		connection.setConnectTimeout(15 * 1000);
		connection.setReadTimeout(15 * 1000);
		connection.setAllowUserInteraction(false);
		connection.setRequestMethod("GET");
		connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36");
		connection.setRequestProperty("Accept", "text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8");
		connection.setRequestProperty("Accept-Encoding", "gzip, deflate");
		connection.connect();
		
		String contentType = connection.getContentType();
		
		long totalBytesRead = 0;
		
		int response = connection.getResponseCode();
		if (response >= 0) {
			String acceptProperty = connection.getContentEncoding();
			if ("gzip".equalsIgnoreCase(acceptProperty)) {
				try (InputStream in = connection.getInputStream();
						BufferedInputStream input = new BufferedInputStream(new GZIPInputStream(in))) {
					totalBytesRead = copy(input, out);
				}
			} else if ("deflate".equalsIgnoreCase(acceptProperty)) {
				try (BufferedInputStream input = new BufferedInputStream(new DeflaterInputStream(connection.getInputStream()))) {
					totalBytesRead = copy(input, out);
				}
			} else {
				try (BufferedInputStream input = new BufferedInputStream(connection.getInputStream())) {
					totalBytesRead = copy(input, out);
				}
			}
		}
		
		if (totalBytesRead <= 0) {
			throw new IOException("Nothing downloaded (downloadPath='" + url + "')");
		}
		
		return contentType;
	}
	
}
