package jtw.util.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class FileToStringReader {

	/**
	 * Reads the content of the file. May add an additional line feed at the end of
	 * the string. All line feeds are converted to \n.
	 * 
	 * @param file
	 *            The file to read.
	 * 
	 * @return The text in the file.
	 * 
	 * @throws IOException
	 */
	public static String read(String path) throws IOException {
		StringBuffer buffer = new StringBuffer();

		File file = new File(path);
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				buffer.append(line).append('\n');
			}
		}

		return buffer.toString();
	}

	/**
	 * Reads the content of the file. May add an additional line feed at the end of
	 * the string. All line feeds are converted to \n.
	 * 
	 * @param file
	 *            The file to read.
	 * 
	 * @return The text in the file.
	 * 
	 * @throws IOException
	 */
	public static String read(URL path) throws IOException {
		StringBuffer buffer = new StringBuffer();

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(path.openStream()))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				buffer.append(line).append('\n');
			}
		}

		return buffer.toString();
	}

}
