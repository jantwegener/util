package jtw.util.rnd;

import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * A class to have a global random number generator.
 * 
 * @author jwegener
 */
public class RND {
    
    private static Random rnd = new Random();
    
    private RND() {
    }
    
    public static Random getRandom() {
    	return rnd;
    }
    
    public static void init(long seed) {
        rnd.setSeed(seed);
    }
    
    /**
     * Returns the next pseudorandom, uniformly distributed int value from this random number generator's sequence. The
     * general contract of nextInt is that one int value is pseudorandomly generated and returned. All 2^32 possible int
     * values are produced with (approximately) equal probability.
     * 
     * @return The next pseudorandom, uniformly distributed int value from this random number generator's sequence.
     */
    public static int nextInt() {
        return rnd.nextInt();
    }
    
    /**
     * Returns the next pseudorandom, uniformly distributed int value from this random number generator's sequence in
     * the interval [min, max).
     * 
     * @param min
     *            The smallest returned number.
     * @param max
     *            The largest (exclusive) returned number.
     * 
     * @return the next pseudorandom, uniformly distributed int value from this random number generator's sequence.
     */
    public static int nextInt(int min, int max) {
        return rnd.nextInt(max - min) + min;
    }
    
    /**
     * Returns the next pseudorandom, uniformly distributed double value between 0.0 and 1.0 from this random number
     * generator's sequence.
     * 
     * @return The next pseudorandom, uniformly distributed double value between 0.0 and 1.0 from this random number
     *         generator's sequence.
     */
    public static double nextDouble() {
        return rnd.nextDouble();
    }
    
    /**
     * Returns the next pseudorandom, uniformly distributed double value between 0.0 and 1.0 from this random number
     * generator's sequence.
     * 
     * @param min
     *            The smallest returned number.
     * @param max
     *            The largest (exclusive) returned number.
     * 
     * @return The next pseudorandom, uniformly distributed double value between 0.0 and 1.0 from this random number
     *         generator's sequence.
     */
    public static double nextDouble(double min, double max) {
        return ((max - min) * rnd.nextDouble()) + min;
    }
    
    /**
     * Returns a pseudorandom object from the given array.
     * 
     * @param arr
     *            The array from which to chose an object.
     * 
     * @return A random object from the array.
     */
    public static <T> T random(T[] arr) {
        T obj = arr[nextInt(0, arr.length)];
        return obj;
    }
    
    /**
     * Returns a pseudorandom object from the given list.
     * 
     * @param list
     *            The list from which to chose an object.
     * 
     * @return A random object from the list.
     */
    public static <T> T random(List<T> list) {
        T obj = list.get(nextInt(0, list.size()));
        return obj;
    }
    
    /**
     * Returns a pseudorandom object from the given set.
     * 
     * @param set
     *            The set from which to chose an object.
     * 
     * @return A random object from the set.
     * 
     * @see Set#toArray()
     */
    public static <T> T random(Set<T> set) {
        @SuppressWarnings("unchecked")
        T obj = (T) set.toArray()[nextInt(0, set.size())];
        return obj;
    }
    
}
