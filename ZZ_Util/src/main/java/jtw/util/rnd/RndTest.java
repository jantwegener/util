package jtw.util.rnd;

public class RndTest {
    
    public static void main(String[] args) {
        // RND.init(5);
        int x = RND.nextInt();
        System.out.println(x);
        x = RND.nextInt();
        System.out.println(x);
        
        x = RND.nextInt(0, 10);
        System.out.println(x);
        x = RND.nextInt(0, 10);
        System.out.println(x);
        
        double y = RND.nextDouble();
        System.out.println(y);
        y = RND.nextDouble();
        System.out.println(y);
        
        y = RND.nextDouble(-10, 5);
        System.out.println(y);
        y = RND.nextDouble(-10, 5);
        System.out.println(y);
    }
    
}
