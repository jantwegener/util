package jtw.util.sound;

/**
 * Class applying the Blackman window function.
 * 
 * Based on the work of <a href="https://rjeschke.tumblr.com/post/8382596050/fir-filters-in-practice">https://rjeschke.tumblr.com/post/8382596050/fir-filters-in-practice</a>.
 * 
 * @author Jan-Thierry Wegener
 *
 */
public class BlackmanWindowFunction extends FIRAbstractWindow {
	
	public BlackmanWindowFunction(FIRFilter filter, boolean fastCalc) {
		super(filter, fastCalc);
	}

	@Override
	public double[] createWindow(double[] fir, boolean fastCalc) {
		final int m = fir.length - 1;
		final double TWO_PI_M = 2.0 * Math.PI / m;
		for (int i = 0; i < fir.length; i++) {
			fir[i] *= 0.42 - 0.5 * SoundMath.cos(TWO_PI_M * i, fastCalc) + 0.08 * SoundMath.cos(2.0 * TWO_PI_M * i, fastCalc);
		}
		return fir;
	}
	
}
