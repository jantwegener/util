package jtw.util.sound;

public abstract class FIRAbstractWindow implements IWindowFunction {
	
	private FIRFilter filter;
	
	private boolean fastCalc;
	
	public FIRAbstractWindow(FIRFilter filter, boolean fastCalc) {
		this.filter = filter;
		this.fastCalc = fastCalc;
	}
	
	public byte[] applyWindow(byte[] input) {
		double[] coeff = filter.createFilter();
		double[] windowCoef = createWindow(coeff, fastCalc);
		
		int fl = windowCoef.length;
		for (int i = 0; i < input.length - fl; i++) {
			double output = 0.0;
			for (int j = 0; j < fl; j++) {
				output += windowCoef[j] * input[i + j];
			}
			input[i] = (byte) output;
		}
		
		return input;
	}
	
}
