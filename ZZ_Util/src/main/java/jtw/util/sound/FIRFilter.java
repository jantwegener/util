package jtw.util.sound;

public abstract class FIRFilter implements IFilter {
	
	private double[] filter;
	
	private int order;
	
	private double cutFrequency;
	
	private double sampleRate;
	
	public FIRFilter(int order, double cutFrequency, double sampleRate) {
		this.order = order;
		this.cutFrequency = cutFrequency;
		this.sampleRate = sampleRate;
		
		setFilter(order, cutFrequency, sampleRate);
	}
	
	/**
	 * Sets the filter to the given order, cut frequency and sample rate.
	 * 
	 * @param order The order of the filter.
	 * @param cutFrequency The cut frequency.
	 * @param sampleRate The sample rate.
	 */
	public void setFilter(int order, double cutFrequency, double sampleRate) {
		filter = createFilter();
	}

	/**
	 * Creates the filter coefficients.
	 * 
	 * @return The filter coefficients.
	 */
	public abstract double[] createFilter();
	
	@Override
	public byte[] filter(byte[] input) {
		int fl = filter.length;
		for (int i = 0; i < input.length - fl; i++) {
			double output = 0.0;
			for (int j = 0; j < fl; j++) {
				output += filter[j] * input[i + j];
			}
			input[i] = (byte) output;
		}
		return input;
	}
	
	/**
	 * Returns the order of the filter.
	 * 
	 * @return The order.
	 */
	public int getOrder() {
		return order;
	}
	
	/**
	 * Returns the cut frequency.
	 * 
	 * @return The cut frequency.
	 */
	public double getCutFrequency() {
		return cutFrequency;
	}
	
	/**
	 * Returns the sample rate.
	 * 
	 * @return The sample rate.
	 */
	public double getSampleRate() {
		return sampleRate;
	}
	
}
