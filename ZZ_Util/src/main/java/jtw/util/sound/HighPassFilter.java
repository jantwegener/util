package jtw.util.sound;

/**
 * Based on the work of https://rjeschke.tumblr.com/post/8382596050/fir-filters-in-practice.
 * 
 * @author Jan-Thierry Wegener
 *
 */
public class HighPassFilter extends FIRFilter {
	
	public HighPassFilter(int order, double cutFrequency, double sampleRate) {
		super(order, cutFrequency, sampleRate);
	}
	
	public double[] createFilter() {
		final double cutoff = getCutFrequency() / getSampleRate();
		final double[] fir = new double[getOrder() + 1];
		final double factor = 2.0 * cutoff;
		final int half = getOrder() >> 1;
		for(int i = 0; i < fir.length; i++) {
			fir[i] = (i == half ? 1.0 : 0.0) 
					- factor * SoundMath.sinc(factor * (i - half), true);
		}
		return fir;
	}
	
}
