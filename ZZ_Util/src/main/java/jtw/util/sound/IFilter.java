package jtw.util.sound;

public interface IFilter {
	
	/**
	 * Filters the given input and returns the result. The input data may also directly be manipulated.
	 * 
	 * @param input The sound input to filter on.
	 * 
	 * @return The filtered data.
	 */
	public byte[] filter(byte[] input);
	
}
