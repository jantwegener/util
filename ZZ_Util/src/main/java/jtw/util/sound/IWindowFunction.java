package jtw.util.sound;

public interface IWindowFunction {
	
	/**
	 * Applies the window on the given array of FIR filter (finite impulse response).
	 * The passed array is directly manipulated and returned.
	 * 
	 * @param fir The FIR filter coefficients.
	 * @param fastCalc true if fast approximation algorithms for sine and cosine shall be used, false if the "exact" value shall be computed.
	 * 
	 * @return The manipulated FIR.
	 */
	public double[] createWindow(double[] fir, boolean fastCalc);
	
	/**
	 * Applies the window to the input.
	 * 
	 * @param input The input to apply the window to.
	 * 
	 * @return The output.
	 */
	public byte[] applyWindow(byte[] input);
	
}
