package jtw.util.sound;

/**
 * Based on the work of https://rjeschke.tumblr.com/post/8382596050/fir-filters-in-practice.
 * 
 * @author Jan-Thierry Wegener
 *
 */
public class LowPassFilter extends FIRFilter {
	
	public LowPassFilter(int order, double cutFrequency, double sampleRate) {
		super(order, cutFrequency, sampleRate);
	}
	
	@Override
	public double[] createFilter() {
		double factor = 2.0 * getCutFrequency() / getSampleRate();
		double[] fir = new double[getOrder() + 1];
		int half = getOrder() >> 1;
		for (int i = 0; i < fir.length; i++) {
			fir[i] = factor * SoundMath.sinc(factor * (i - half), true);
		}
		return fir;
	}
	
}
