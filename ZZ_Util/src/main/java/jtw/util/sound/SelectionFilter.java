package jtw.util.sound;

public class SelectionFilter implements IFilter {
	
	private int skip = 0;
	
	public SelectionFilter(int skip) {
		this.skip = skip;
	}
	
	/**
	 * Filters the given input and returns the result. The input data is not manipulated.
	 * 
	 * @param input The sound input to filter on.
	 * 
	 * @return The filtered data.
	 */
	@Override
	public byte[] filter(byte[] input) {
		byte[] b = new byte[input.length / skip];
		for (int i = 0; i < b.length; i++) {
			b[i] = input[i * skip];
		}
		return b;
	}
	
}
