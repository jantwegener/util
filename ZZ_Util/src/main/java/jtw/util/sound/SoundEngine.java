package jtw.util.sound;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class SoundEngine implements Runnable {
	
	/**
	 * The sample rate.
	 */
	private static final int SAMPLE_RATE = 44100;
	
	private static final int MIN_AVAILABLE = SAMPLE_RATE / 4;

	private static final long SLEEP_TIME = 50;
	
	/**
	 * The sine wave form.
	 */
	public static final int WAVE_SINE = 1;
	
	/**
	 * The square wave form.
	 */
	public static final int WAVE_SQUARE = 2;
	
	/**
	 * The custom wave form.
	 */
	public static final int WAVE_CUSTOM = 3;

	private final AudioFormat audioFormat;
	
	private SourceDataLine line;
	
	private volatile Thread thread;

	private double frequency = -1;
	
	/**
	 * The duration of the tone created. By default it is 250 ms.
	 */
	private int duration = 250;
	
	private int waveForm = WAVE_SINE;
	
	private double[] customWaveForm;
	
	/**
	 * The volume in the range 0 to 1, where 0 is silent and 1 is loudest.
	 */
	private double volume = 1;
	
	public SoundEngine() throws LineUnavailableException {
		this(SAMPLE_RATE);
	}

	public SoundEngine(int sampleRate) throws LineUnavailableException {
		audioFormat = new AudioFormat(sampleRate, 8, 1, true, true);
		line = AudioSystem.getSourceDataLine(audioFormat);
	}
	
	public void openAndStartLine() throws LineUnavailableException {
		line.open(audioFormat, SAMPLE_RATE);
		line.start();
	}
	
	public void start() throws LineUnavailableException {
		if (thread == null) {
			openAndStartLine();
			
			thread = new Thread(this);
			thread.start();
		}
	}
	
	public void stop() {
		thread = null;
		
		line.drain();
		line.stop();
		line.close();
	}
	
	public void pause() {
		if (line.isRunning()) {
			line.flush();
			line.stop();
		}
	}
	
	public void resume() {
		if (!line.isRunning()) {
			line.start();
		}
	}
	
	public void run() {
		while (thread == Thread.currentThread()) {
			if (line.available() > MIN_AVAILABLE && frequency > 0) {
				byte[] b = generateWave();
				line.write(b, 0, b.length);
			}
			
			try {
				Thread.sleep(SLEEP_TIME);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public byte[] generateWave() {
		byte[] b;
		switch (waveForm) {
			case WAVE_SINE:
				b = computeSineWave();
				break;
			case WAVE_SQUARE:
				b = computeSquareWave();
				break;
			case WAVE_CUSTOM:
				b = computeCustomWave();
				break;
				
			default:
				throw new RuntimeException("Unsupported wave form");
		}
		return b;
	}
	
	public void play(byte[] b) {
		line.write(b, 0, b.length);
	}
	
	public int getSampleRate() {
		return SAMPLE_RATE;
	}
	
	public void flush() {
		line.stop();
		line.flush();
		line.start();
	}

	/**
	 * Sets the volume in the range 0 to 1.
	 * 
	 * @param volume 0 is silent and 1 is loudest.
	 */
	public void setVolume(double volume) {
		this.volume = volume;
	}
	
	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}
	
	public double getFrequency() {
		return frequency;
	}
	
	public void setDuration(int durationInMS) {
		this.duration = durationInMS;
	}
	
	public int getDuration() {
		return duration;
	}
	
	/**
	 * Sets the custom wave form. The values must be between 0 and 1.<p>
	 * 
	 * A saw wave form can be created by {0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0}.
	 * 
	 * @param customWaveForm The custom wave form.
	 */
	public void setCustomWaveForm(double[] customWaveForm) {
		this.customWaveForm = customWaveForm;
	}
	
	private byte[] computeSineWave() {
		int samples = (duration * SAMPLE_RATE) / 1000;
		
		byte[] b = new byte[samples];
		
		double invPeriod = frequency / SAMPLE_RATE;
		
		for (int i = 0; i < b.length; i++) {
			double angle = 2.0 * Math.PI * i * invPeriod;
			b[i] = (byte)(SoundMath.sin(angle, true) * 127d * volume);
		}
		
		return b;
	}
	
	private byte[] computeSquareWave() {
		int samples = (duration * SAMPLE_RATE) / 1000;
		
		byte[] b = new byte[samples];
		
		double invPeriod = frequency / SAMPLE_RATE;
		
		for (int i = 0; i < b.length; i++) {
			double angle = 2.0 * Math.PI * i * invPeriod;
			b[i] = (byte)(Math.signum(SoundMath.sin(angle, true)) * 127d * volume);
		}
		
		return b;
	}
	
	private byte[] computeCustomWave() {
		int samples = (duration * SAMPLE_RATE) / 1000;
		
		byte[] b = new byte[samples];
		
		int period = (int)(SAMPLE_RATE / frequency / customWaveForm.length);
		if (period <= 0) {
			period = 1;
		}
		int pointer = 0;
		for (int i = 0; i < b.length; i++) {
			if ((i % period) == 0) {
				pointer = (pointer + 1) % customWaveForm.length;
			}
			b[i] = (byte)(customWaveForm[pointer] * 127d * volume);
		}
		
		return b;
	}
	
	public void setWaveForm(int waveForm) {
		this.waveForm  = waveForm;
	}
	
}
