package jtw.util.sound;

public class SoundMath {
	
	/**
	 * The constant factor to scale down to a maximum of 1 in the range 0 to 2pi for sine.
	 */
	private final static double UNI = 20.785 / (8.0 * Math.PI * Math.PI * Math.PI);

	/**
	 * Computes sine of the given angle. This method can compute an approximation if second parameter is set to <code>true</code>.
	 * 
	 * @param x The angle.
	 * @param fastSine <code>true</code> if an approximation value is allowed, <code>false</code> otherwise.
	 * 
	 * @return The sine of x.
	 * 
	 * @see <a href="https://www.youtube.com/watch?v=1xlCVBIF_ig">https://www.youtube.com/watch?v=1xlCVBIF_ig</a>
	 */
	public static double sin(double x, boolean fastSine) {
		if (!fastSine) {
			return Math.sin(x);
		}
		
		while (x < 0) {
			x += 2 * Math.PI;
		}
		while (x > 2 * Math.PI) {
			x -= 2 * Math.PI;
		}
		
		double sine = UNI * (x - 0.0) * (x - Math.PI) * (x - 2 * Math.PI);
		return sine;
	}

	/**
	 * Computes cosine of the given angle. This method can compute an approximation if second parameter is set to <code>true</code>.
	 * 
	 * @param x The angle.
	 * @param fastCosine <code>true</code> if an approximation value is allowed, <code>false</code> otherwise.
	 * 
	 * @return The cosine of x.
	 * 
	 * @see <a href="https://www.youtube.com/watch?v=1xlCVBIF_ig">https://www.youtube.com/watch?v=1xlCVBIF_ig</a>
	 */
	public static double cos(double x, boolean fastCosine) {
		return sin(0.5 * Math.PI - x, fastCosine);
	}
	
	/**
	 * Computes sin(x*pi) / (x*pi).
	 * 
	 * @param x The angle
	 * @param fastSine <code>true</code> if an approximation value is allowed, <code>false</code> otherwise.
	 * 
	 * @return The sinc of x.
	 * 
	 * @see <a href="https://rjeschke.tumblr.com/post/8382596050/fir-filters-in-practice">https://rjeschke.tumblr.com/post/8382596050/fir-filters-in-practice</a>
	 */
	public static double sinc(double x, boolean fastSine) {
		double retval = 1.0;
		if (x != 0) {
			double xpi = Math.PI * x;
			retval = SoundMath.sin(xpi, fastSine) / xpi;
		}
		return retval;
	}
	
}
