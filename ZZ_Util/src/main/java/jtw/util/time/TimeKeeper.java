package jtw.util.time;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TimeKeeper {
	
	private static Map<String, List<Long>> timeMap = new HashMap<String, List<Long>>();
	
	private TimeKeeper() {
	}
	
	/**
	 * Resets and initializes the timer.
	 * 
	 * @param name The name of the timer.
	 */
	public static void reset(String name) {
		List<Long> list = timeMap.get(name);
		if (list == null) {
			list = new ArrayList<>();
			timeMap.put(name, list);
		}
		list.clear();
	}
	
	/**
	 * Resets and starts the measurement.
	 * 
	 * @param name The name of the timer.
	 */
	public static void start(String name) {
		reset(name);
		// reset already created the list if it did not exist before
		List<Long> list = timeMap.get(name);
		list.add(System.currentTimeMillis());
	}
	
	/**
	 * Adds the next lap to the timer.
	 * 
	 * The method start must have been called before using this method. Otherwise, a {@link NullPointerException} will be thrown.
	 * 
	 * @param name The name of the timer.
	 * 
	 * @see #start(String)
	 */
	public static void lap(String name) {
		List<Long> list = timeMap.get(name);
		list.add(System.currentTimeMillis());
	}
	
	/**
	 * Stops the timer.
	 * 
	 * The method start must have been called before using this method. Otherwise, a {@link NullPointerException} will be thrown.
	 * 
	 * @param name The name of the timer.
	 */
	public static void stop(String name) {
		List<Long> list = timeMap.get(name);
		list.add(System.currentTimeMillis());
	}
	
	/**
	 * Prints the results of the timer.
	 * 
	 * To calculate a result, at least the methods {@link #start(String)} and {@link #stop(String)} must have been called.
	 * 
	 * The method start must have been called before using this method. Otherwise, a {@link NullPointerException} will be thrown.
	 * 
	 * @param name The name of the timer.
	 */
	public static void printResult(String name) {
		System.out.println("Results for " + name + ": ");
		List<Long> list = timeMap.get(name);
		long start = list.get(0);
		for (int i = 1; i < list.size(); i++) {
			long end = list.get(i);
			System.out.println("> " + (end - start) + " ms");
			start = end;
		}
	}
	
	/**
	 * Prints all results.
	 */
	public static void printAllResults() {
		for (String str : timeMap.keySet()) {
			printResult(str);
		}
	}

}
