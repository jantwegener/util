package jtw.graph.algorithm.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.List;

import org.junit.jupiter.api.Test;

import jtw.graph.DirectedArc;
import jtw.graph.Graph;
import jtw.graph.Node;
import jtw.graph.algorithm.BreadthFirstPathFinder;
import jtw.graph.algorithm.NodeNotFoundException;

public class BreadthFirstPathFinderTest {

	@Test
	public void testFirstFind() throws NodeNotFoundException {
		Graph<Object, Object> tree = createTestTree();
		BreadthFirstPathFinder<Object, Object> pathFinder = new BreadthFirstPathFinder<>();
		List<DirectedArc<Object, Object>> path = pathFinder.findPath(tree, tree.getNode("N0"), tree.getNode("N1"));
		assertNotNull(path);
		assertFalse(path.isEmpty());
		assertEquals(1, path.size());
	}

	@Test
	public void testFirstStartEqualEnd() throws NodeNotFoundException {
		Graph<Object, Object> tree = createTestTree();
		BreadthFirstPathFinder<Object, Object> pathFinder = new BreadthFirstPathFinder<>();
		List<DirectedArc<Object, Object>> path = pathFinder.findPath(tree, tree.getNode("N4"), tree.getNode("N4"));
		assertNotNull(path);
		assertTrue(path.isEmpty());
		assertEquals(0, path.size());
	}

	@Test
	public void testTreeFind() throws NodeNotFoundException {
		Graph<Object, Object> tree = createTestTree();
		BreadthFirstPathFinder<Object, Object> pathFinder = new BreadthFirstPathFinder<>();
		List<DirectedArc<Object, Object>> path = pathFinder.findPath(tree, tree.getNode("N0"), tree.getNode("N9"));
		assertNotNull(path);
		assertFalse(path.isEmpty());
		assertEquals(4, path.size());
	}

	@Test
	public void testTreeNoPathFind() throws NodeNotFoundException {
		Graph<Object, Object> tree = createTestTree();
		BreadthFirstPathFinder<Object, Object> pathFinder = new BreadthFirstPathFinder<>();
		List<DirectedArc<Object, Object>> path = pathFinder.findPath(tree, tree.getNode("N9"), tree.getNode("N0"));
		assertNotNull(path);
		assertTrue(path.isEmpty());
	}

	@Test
	public void testCyclicGraphEasyFind() throws NodeNotFoundException {
		Graph<Object, Object> tree = createTestCyclicGraph();
		BreadthFirstPathFinder<Object, Object> pathFinder = new BreadthFirstPathFinder<>();
		List<DirectedArc<Object, Object>> path = pathFinder.findPath(tree, tree.getNode("N0"), tree.getNode("N9"));
		assertNotNull(path);
		assertFalse(path.isEmpty());
		assertEquals(4, path.size());
	}

	@Test
	public void testCyclicGraphHard1Find() throws NodeNotFoundException {
		Graph<Object, Object> tree = createTestCyclicGraph();
		BreadthFirstPathFinder<Object, Object> pathFinder = new BreadthFirstPathFinder<>();
		List<DirectedArc<Object, Object>> path = pathFinder.findPath(tree, tree.getNode("N1"), tree.getNode("N9"));
		assertNotNull(path);
		assertFalse(path.isEmpty());
		assertEquals(5, path.size());
	}

	@Test
	public void testCyclicGraphHard2Find() throws NodeNotFoundException {
		Graph<Object, Object> tree = createTestCyclicGraph();
		BreadthFirstPathFinder<Object, Object> pathFinder = new BreadthFirstPathFinder<>();
		List<DirectedArc<Object, Object>> path = pathFinder.findPath(tree, tree.getNode("N3"), tree.getNode("N8"));
		assertNotNull(path);
		assertFalse(path.isEmpty());
		assertEquals(6, path.size());
	}

	@Test
	public void testCyclicGraphHard3Find() throws NodeNotFoundException {
		Graph<Object, Object> tree = createTestCyclicGraph();
		BreadthFirstPathFinder<Object, Object> pathFinder = new BreadthFirstPathFinder<>();
		List<DirectedArc<Object, Object>> path = pathFinder.findPath(tree, tree.getNode("N8"), tree.getNode("N4"));
		assertNotNull(path);
		assertFalse(path.isEmpty());
		assertEquals(5, path.size());
	}

	private Graph<Object, Object> createTestTree() throws NodeNotFoundException {
		Graph<Object, Object> g = new Graph<>();
		for (int i = 0; i < 10; i++) {
			Node<Object> n = new Node<>("N" + i);
			g.addNode(n);
		}
		g.addArc(new DirectedArc<>(g.getNode("N0"), g.getNode("N1"), 1));
		g.addArc(new DirectedArc<>(g.getNode("N0"), g.getNode("N2"), 1));
		g.addArc(new DirectedArc<>(g.getNode("N0"), g.getNode("N3"), 1));

		g.addArc(new DirectedArc<>(g.getNode("N1"), g.getNode("N4"), 1));

		g.addArc(new DirectedArc<>(g.getNode("N2"), g.getNode("N5"), 1));
		g.addArc(new DirectedArc<>(g.getNode("N2"), g.getNode("N6"), 1));

		g.addArc(new DirectedArc<>(g.getNode("N5"), g.getNode("N7"), 1));

		g.addArc(new DirectedArc<>(g.getNode("N6"), g.getNode("N8"), 1));
		
		g.addArc(new DirectedArc<>(g.getNode("N8"), g.getNode("N9"), 1));
		
		return g;
	}

	private Graph<Object, Object> createTestCyclicGraph() throws NodeNotFoundException {
		Graph<Object, Object> g = new Graph<>();
		for (int i = 0; i < 10; i++) {
			Node<Object> n = new Node<>("N" + i);
			g.addNode(n);
		}
		g.addArc(new DirectedArc<>(g.getNode("N0"), g.getNode("N1"), 1));
		g.addArc(new DirectedArc<>(g.getNode("N0"), g.getNode("N2"), 1));
		g.addArc(new DirectedArc<>(g.getNode("N0"), g.getNode("N3"), 1));

		g.addArc(new DirectedArc<>(g.getNode("N1"), g.getNode("N4"), 1));

		g.addArc(new DirectedArc<>(g.getNode("N2"), g.getNode("N5"), 1));
		g.addArc(new DirectedArc<>(g.getNode("N2"), g.getNode("N6"), 1));

		g.addArc(new DirectedArc<>(g.getNode("N5"), g.getNode("N7"), 1));

		g.addArc(new DirectedArc<>(g.getNode("N6"), g.getNode("N8"), 1));
		
		g.addArc(new DirectedArc<>(g.getNode("N8"), g.getNode("N9"), 1));
		
		// making the tree a cyclic graph
		g.addArc(new DirectedArc<>(g.getNode("N7"), g.getNode("N4"), 1));
		g.addArc(new DirectedArc<>(g.getNode("N2"), g.getNode("N0"), 1));
		g.addArc(new DirectedArc<>(g.getNode("N9"), g.getNode("N3"), 1));
		g.addArc(new DirectedArc<>(g.getNode("N3"), g.getNode("N5"), 1));
		g.addArc(new DirectedArc<>(g.getNode("N4"), g.getNode("N2"), 1));
		
		return g;
	}
	
	
	
}
