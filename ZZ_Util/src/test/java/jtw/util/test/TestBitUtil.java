package jtw.util.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import jtw.util.BitUtil;

public class TestBitUtil {
	
	@Test
	public void testToInt() {
		int d5 = BitUtil.toInt(true, false, true);
		assertEquals(5, d5);
		d5 = BitUtil.toInt(false, true, false, true);
		assertEquals(5, d5);
		int d1602 = BitUtil.toInt(true, true, false, false, true, false, false, false, false, true, false);
		assertEquals(1602, d1602);
		int d51586 = BitUtil.toInt(true, true, false, false, true, false, false, true, true, false, false, false, false, false, true, false);
		assertEquals(51586, d51586);
	}
	
	@Test
	public void testIsBitSet() {
		// 5
		boolean t = BitUtil.isBitSet(5, 2);
		assertEquals(true, t);
		boolean f = BitUtil.isBitSet(5, 1);
		assertEquals(false, f);
		// 51586
		t = BitUtil.isBitSet(51586, 1);
		assertEquals(true, t);
		t = BitUtil.isBitSet(51586, 8);
		assertEquals(true, t);
		t = BitUtil.isBitSet(51586, 11);
		assertEquals(true, t);
		f = BitUtil.isBitSet(51586, 0);
		assertEquals(false, f);
		f = BitUtil.isBitSet(51586, 31);
		assertEquals(false, f);
		// -2147483648
		t = BitUtil.isBitSet(-2147483648, 31);
		assertEquals(true, t);
	}
	
	@Test
	public void testFlipBit() {
		int d5 = BitUtil.flipBit(1, 2);
		assertEquals(5, d5);
		int d32773 = BitUtil.flipBit(d5, 15);
		assertEquals(32773, d32773);
		d5 = BitUtil.flipBit(d32773, 15);
		assertEquals(5, d5);
	}
	
	@Test
	public void testSetBit() {
		int d5 = BitUtil.setBit(1, 2);
		assertEquals(5, d5);
		int d32773 = BitUtil.setBit(d5, 15);
		assertEquals(32773, d32773);
		d32773 = BitUtil.setBit(d32773, 15);
		assertEquals(32773, d32773);
		d32773 = BitUtil.setBit(d32773, 0);
		assertEquals(32773, d32773);
	}
	
	@Test
	public void testClearBit() {
		int d5 = BitUtil.clearBit(7, 1);
		assertEquals(5, d5);
		int d32773 = BitUtil.clearBit(98309, 16);
		assertEquals(32773, d32773);
		d5 = BitUtil.clearBit(5, 1);
		assertEquals(5, d5);
	}
	
	@Test
	public void testTo8Bit() {
		int d255 = BitUtil.to8Bit(-1);
		assertEquals(255, d255);
		int dd224 = BitUtil.to8Bit(0xd224);
		assertEquals(0x24, dd224);
		int d5 = BitUtil.to8Bit(5);
		assertEquals(5, d5);
		int d0 = BitUtil.to8Bit(0xFFFF_FF00);
		assertEquals(0, d0);
	}
	
	@Test
	public void testTo8BitByte() {
		byte b = -1;
		int d255 = BitUtil.to8Bit(b);
		assertEquals(255, d255);
		b = -128;
		int dd224 = BitUtil.to8Bit(b);
		assertEquals(0x80, dd224);
		b = 5;
		int d5 = BitUtil.to8Bit(b);
		assertEquals(5, d5);
		b = 0x7F;
		int d0 = BitUtil.to8Bit(b);
		assertEquals(0x7F, d0);
	}
	
	@Test
	public void testTo16Bit() {
		int d255 = BitUtil.to16Bit(-1);
		assertEquals(0xffff, d255);
		int dd224 = BitUtil.to16Bit(0xd224);
		assertEquals(0xd224, dd224);
		int d5 = BitUtil.to16Bit(5);
		assertEquals(5, d5);
		int d0 = BitUtil.to16Bit(0xFFFF_0000);
		assertEquals(0, d0);
	}
	
	@Test
	public void testTo16BitLE() {
		int d255 = BitUtil.to16BitLE(-1);
		assertEquals(0xffff, d255);
		int dd224 = BitUtil.to16BitLE(0xd224);
		assertEquals(0x24d2, dd224);
		int d5 = BitUtil.to16BitLE(5);
		assertEquals(0x0500, d5);
		int d0 = BitUtil.to16BitLE(0x00fa_0000);
		assertEquals(0, d0);
	}
	
	@Test
	public void testTo32BitLE() {
		int d255 = BitUtil.to32BitLE(-1);
		assertEquals(0xffff_ffff, d255);
		int dd224 = BitUtil.to32BitLE(0xd224);
		assertEquals(0x24d2_0000, dd224);
		int d5 = BitUtil.to32BitLE(5);
		assertEquals(0x0500_0000, d5);
		int d0 = BitUtil.to32BitLE(0x0000_FABF);
		assertEquals(0xbffa_0000, d0);
		int d0xdeadbeef = BitUtil.to32BitLE(0xdeadbeef);
		assertEquals(0xefbeadde, d0xdeadbeef);
	}
	
	@Test
	public void testTo16BitFromLsbMsb() {
		int d12ab = BitUtil.to16Bit(0x12, 0xab);
		assertEquals(0x12ab, d12ab);
		int dd224 = BitUtil.to16Bit(0xd2, 0x24);
		assertEquals(0xd224, dd224);
		int d5 = BitUtil.to16Bit(0x05, 0x60);
		assertEquals(0x0560, d5);
		int d0 = BitUtil.to16Bit(0xaFFF, 0x1000);
		assertEquals(0xff00, d0);
	}
	
	@Test
	public void testTo16BitFromLsbMsbLE() {
		int d12ab = BitUtil.to16BitLE(0xab, 0x12);
		assertEquals(0x12ab, d12ab);
		int dd224 = BitUtil.to16BitLE(0x24, 0xd2);
		assertEquals(0xd224, dd224);
		int d5 = BitUtil.to16BitLE(0x60, 0x05);
		assertEquals(0x0560, d5);
		int d0 = BitUtil.to16BitLE(0x1000, 0xafff);
		assertEquals(0xff00, d0);
	}
	
}
